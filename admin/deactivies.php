<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
//error_reporting(E_ALL);
// if ($_SESSION['ADMIN_TYPE'] != "admin") {
//     header("Location:dashboard");
// }
$commonAppApi = new Common($dbconn);

$contactname="";
$email="";
$status="";
if (isset($_POST["contact"])) {
    $contactname = trim($_POST["contact"]);
}
if (isset($_POST["email"])) {
    $email = trim($_POST["email"]);
}
if (isset($_POST["status"])) {
    $status = trim($_POST["status"]);
}

if (isset($_GET["id"])) {
    $delete_id = $commonAppApi->decode($_GET["id"]);
    $qryParams = array ( 
                ":user_id" => $delete_id,
                ":status" => "1"
            );
    $setCondtn = $commonAppApi->funParseQryParams($qryParams, "status",","); 
    $reqQryParams = array (
            "tableName" => "tbl_users",
            "setCondtn" =>$setCondtn,
            "whereCondition" => "user_id=:user_id"
        );

    $updateQryResponse = $commonAppApi->funExeUpdateRecord($reqQryParams, $qryParams);
    header("Location:deactivies?msg=".$commonAppApi->encode('3'));
    exit;
}
$AlertMessage = '';
$AlertClass = '';
$AlertFlag = false;
if (isset($_GET['msg'])) { 
    if ($commonAppApi->decode($_GET['msg']) == 1) {
        $AlertMessage = "Labour added successfully.";
        $AlertClass = "alertsuccess";
        $AlertFlag = true;
    }
    if ($commonAppApi->decode($_GET['msg']) == 2) {
        $AlertMessage = "Labour updated successfully.";
        $AlertClass = "alertsuccess";
        $AlertFlag = true;
    }
    if ($commonAppApi->decode($_GET['msg']) == 3) {
        $AlertMessage = "Labour activated successfully.";
        $AlertClass = "alertsuccess";
        $AlertFlag = true;
    }

}
/****Paging ***/
$Page = 1; $RecordsPerPage = 25;
if(isset($_REQUEST['HdnPage']) && is_numeric($_REQUEST['HdnPage']))
    $Page = $_REQUEST['HdnPage'];
$TotalPages = 0;
/*End of paging*/
include("header.php");

?>
<form name="active_list_form" id="active_list_form" method="post" action="">
    <div class="page-content" id="contact-list-page-content">
    	<div class="row food-orders">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="portlet light customlistminheight">
                    <div class="portlet-title" >
                        <div class="caption font-dark caption-new">
                            <img src="../assets/layouts/layout2/img/setting-25x25-1.png" class="icon-img">
                            <span class="caption-subject bold uppercase icon-title-name">Deactivies</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                    <?php if ($AlertFlag == true) { ?>
                    <div class="alert alert-block fade in <?php echo $AlertClass; ?>">
                        <button type="button" class="close" data-dismiss="alert"></button>
                        <p> <?php echo $AlertMessage; ?> </p>
                    </div>
                    <?php } ?>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 reportcontactsearch" id="contactListresponsive">
                                <div class="col-md-6 col-sm-6 col-xs-12 remove-left-right-padding">
                                     <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label>Name:</label>
                                         <input type="text" name="search_name" id="search_name" class="form-control" placeholder="Name" value="<?php echo $search_name ?>">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label>Email:</label>
                                        <input type="text" name="search_email" id="search_email" class="form-control" placeholder="Email" value="<?php echo $search_email ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 remove-left-right-padding">
                                    <div class="col-md-12 col-sm-12 col-xs-12 search-orderlist-btns remove-left-right-padding">
                                        <button type="button" class="btn yellow custombtn" id="Search"><i class="fa fa-search"></i> Search</button>
                                        <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Reset</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="loadingreportsection">
                        <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12 alertmsg">
                        <div class="successmsg"></div>
                    </div>
                    <div id="active_list_table"> 
                        <input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
                        <input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
                        <input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>"> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include_once("footer.php"); ?>
<script src="../assets/layouts/layout2/scripts/active.js" type="text/javascript"></script>
<script src="../assets/global/scripts/easy-responsive-tabs.js" type="text/javascript"></script>
<script>
    var gettempID;
    var collapseID;
    var gettempChildID;
    var getChildID;
    $(document).on("click",".plusicon",function(){
          var getID=$(this).attr("data-target");
        if(gettempID!=getID){
            $(gettempID).removeClass("in");
            $(gettempID).addClass("collapsed");
            $(gettempID).attr("aria-expanded","false");
        }
        if(gettempChildID!=""){
            $(gettempChildID).removeClass("in");
            $(gettempChildID).addClass("collapsed");
            $(gettempChildID).attr("aria-expanded","false");
        }
        if (getID!="") {
            gettempID=getID;
        }
    });

    $(document).on("click",".childplusicon",function(){
          var getChildID=$(this).attr("data-target");
          
         if(gettempChildID!=getChildID) {
            $(gettempChildID).removeClass("in");
            $(gettempChildID).addClass("collapsed");
            $(gettempChildID).attr("aria-expanded","false");
         }
          if(getChildID!="") {
            gettempChildID=getChildID;
          }
    });

    $(document).on("click","#Commentpopup", function (){
        $("#comment-error").hide();
        $("#complete-error").hide();
        $(".dispmsg").css("display","none");
        var userTaskId=$(this).attr("usertask-id");
        var CommentTask = "taskcomment";
        $("#hnduserid").val(userTaskId);
        if (userTaskId != "") {
            $.ajax({
                url:"commonajax.php",
                method:'POST',
                data:"UserTaskId="+userTaskId+"&CommentTask="+CommentTask,
                dataType: 'json',
                success:function(data) { 
                    $("#user_comment").html(data.comment);
                    if (data.complete == true) {
                        $("#statusyes").attr('checked','checked');
                    } else {
                        $("#statusno").attr('checked','checked');
                    }
                }
            });            
        } 
    });

    $(document).on("change blur keyup","#user_comment",function (){
        var comments=$("#user_comment").val();
        if (comments=="") {
            $("#comment-error").show();
            return false;
        } else {
            $("#comment-error").hide();
            return true;
        }
    });

    $(document).on("click","#commentupdate", function (){
        var usertaskId=$("#hnduserid").val();
        var comments=$("#user_comment").val();
        var status=$("input[name=complete]:checked").val();
        if (comments=="") {
          $("#comment-error").show();
          return false;
        } else {
          $("#comment-error").hide();
        }
        if (status=="") {
          $("#complete-error").show();
          return false;
        }  else {
           $("#complete-error").hide();
        }
        if (usertaskId != "" && comments!="" && status!="") {
            $.ajax({
                url:"commonajax.php",
                method:'POST',
                data:"usertaskId="+usertaskId+"&comments="+comments+"&status="+status,
                dataType: 'json',
                success:function(data) { 
                    if(data["message"]="success") {
                        $('#myComment').modal('show');
                        $(".dispmsg").css("display", "block");
                        $("#appendmsg").empty().append("Comments updated successfully");
                        $(".taskcomment_"+usertaskId).empty().append(data["comment"]);
                        $(".taskstatus_"+usertaskId).empty().append(data["status"]);
                        setTimeout(function() {$('#myComment').modal('hide');$("#comment_form")[0].reset();}, 4000);
                    } else {
                       $("#appendmsg").empty().append("Comments not updated");
                       $(".taskcomment_"+usertaskId).empty().append("");
                       $(".taskstatus_"+usertaskId).empty().append(" ");
                    }
                }
            });            
        } 
    });
</script>
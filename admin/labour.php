<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
$commonAppApi = new Common($dbconn);
// if ($_SESSION['ADMIN_TYPE'] != "admin") {
//     header("Location:dashboard");
// }
// $activeStateQryParams = array (":active" =>1);
// $whereStateCondtnActive = $commonAppApi->funParseQryParams($activeStateQryParams, "active", "AND");
// $stateReqQryParams = array (
//     "selectField" => "*",
//     "tableName" => "pro_states",
//     "orderByCondtn" => "state_id",
//     "whereCondition" => $whereStateCondtnActive,
//     "fetchType" => "multiple"
// );
// $stateInfo = $commonAppApi->funExeSelectQuery ($stateReqQryParams, $activeStateQryParams);
 
$mode = "Add";
if (isset($_GET['id']) && ($_GET['id']) != "") {
    
    $selQryParam = array (":user_id" =>$commonAppApi->decode($_GET['id']));
    $whereCondtn = $commonAppApi->funParseQryParams($selQryParam, "user_id"); 
    $reqQryParam = array (
                        "fetchType" => "singlerow",
                        "selectField" => "",
                        "tableName" => "tbl_users",
                        "whereCondition" => $whereCondtn
                    );
    $getCoachRes = $commonAppApi->funExeSelectQuery($reqQryParam, $selQryParam);
    $user_id = $getCoachRes["user_id"];
    $first = $getCoachRes["first"];
    $last = $getCoachRes["last"];
    $email = $getCoachRes["email"];
    $password = $commonAppApi->decode($getCoachRes["password"]);
    $gender =  $getCoachRes["gender"];
    $phone = $getCoachRes["phone"];
    $status = $getCoachRes["status"];
    $mode = "Edit";
}

if (isset($_POST['labour_first']) && ($_POST['labour_first']) != "" && isset($_POST['labour_email']) && $_POST['labour_email']!="") {
    if ($mode == "Add") {

        $selQryParams = array (":email" => $_POST["labour_email"]);
        $whereCondtn = $commonAppApi->funParseQryParams($selQryParams, "email", "AND"); 
        $reqQryParams = array (
                        "fetchType" => "singlerow",
                        "selectField" => "count(email) as countRows, user_id",
                        "tableName" => "tbl_users",
                        "whereCondition" => $whereCondtn 
                    );
        $chekEmailExistRes = $commonAppApi->funExeSelectQuery($reqQryParams, $selQryParams);

        if ( $chekEmailExistRes["countRows"] == 0 ) {
            $insQryParams = array (
                                ":first" => trim($_POST["labour_first"]),
                                ":last" => trim($_POST["labour_last"]),
                                ":gender" => trim($_POST["gender"]),
                                ":email" => trim(strtolower($_POST["labour_email"])),
                                ":type" => "labour",
                                ":phone" => trim($_POST["phone"]),
                                ":status" => $_POST["status"],
                            );
            $insQryResponse = $commonAppApi->funExeInsertRecord("tbl_users", $insQryParams);
            if (!empty($insQryResponse)) {
                header("Location:labours?msg=".$commonAppApi->encode('1'));
                exit;
            }   
        } else {
            header("Location:labour?msg=".$commonAppApi->encode('2'));
            exit;
        }
    } else {
        $qryParams = array (":first" => trim($_POST["labour_first"]),
                            ":last" => trim($_POST["labour_last"]),
                            ":gender" => trim($_POST["gender"]),
                            ":email" => trim(strtolower($_POST["labour_email"])),
                            ":type" => "labour",
                            ":phone" => trim($_POST["phone"]),
                            ":status" => $_POST["status"],
                            ":user_id" => $user_id
                            );
        $setCondtn = $commonAppApi->funParseQryParams($qryParams, "user_id", ",");
        $reqQryParams = array (
                            "tableName" => "tbl_users",
                            "setCondtn" => $setCondtn,
                            "whereCondition" => "user_id=:user_id" );
        $updateQryResponse = $commonAppApi->funExeUpdateRecord($reqQryParams, $qryParams);
        header("Location:labours?msg=".$commonAppApi->encode('2'));
        exit;
    }
}

/*End of paging*/
include("header.php");

?>
<form name="labour_form" id="labour_form" method="post" action="">
<input type="hidden" name="hdncoachid" value="<?php echo $_GET['id']; ?>">
<div class="page-content" id="contact-list-page-content">
    <div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="portlet light customlistminheight">
                <div class="portlet-title" >
                    <div class="caption font-dark caption-new">
                        <img src="../assets/layouts/layout2/img/labours-25x25-1.png" class="icon-img">
                        <span class="caption-subject bold uppercase icon-title-name"><?php echo $mode ?> labour</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <?php
                    if ($commonAppApi->decode($_GET['msg']) == 2) { ?>
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert"></button>
                        <p>You have email already exist, try to another email!!!</p>
                    </div>
                <?php } ?>
                 <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcontactsearch" id="contactListresponsive">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><strong>First:</strong></label>
                                     <input type="text" name="labour_first" id="labour_first" class="form-control" placeholder="First" value="<?php echo $first ?>">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><strong>Last:</strong></label>
                                     <input type="text" name="labour_last" id="labour_last" class="form-control" placeholder="Last" value="<?php echo $last ?>">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><strong>Email:</strong></label>
                                    <input type="text" name="labour_email" id="labour_email" class="form-control" placeholder="Email" value="<?php echo $email ?>">
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 contact-add-padding">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><strong>Gender:</strong></label>
                                    <select  name="gender" id="gender" class="form-control">
                                        <option value="">Select Gender</option>
                                        <option value="M">Male</option>
                                        <option value="F">Female</option>
                                    </select>
                                    <script>$("#gender").val("<?php echo $gender; ?>")</script>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><strong>Phone:</strong></label>
                                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone Number" value="<?php echo $phone ?>">
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><strong>Status:</strong></label><br/>
                                    <label class="radio-inline"><input type="radio" name="status" <?php if($status=="1") {echo "checked";} ?> value="1">Active</label>
                                    <label class="radio-inline"><input type="radio" name="status" <?php if($status=="0") {echo "checked";} ?> value="0">In-Active</label><br>
                                    <label id="status-error" class="error" for="status" style="display: none;">Please select status</label>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 contact-add-padding">
                                <div class="col-md-6 col-sm-6 col-xs-6 contact-save">
                                    <button type="submit" class="btn yellow custombtn"><i class="fa fa-floppy-o"></i> <?php echo $mode ?></button>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <a href="../admin/labours">
                                    <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Cancel</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
<script src="../assets/layouts/layout2/scripts/custom.js" type="text/javascript"></script>
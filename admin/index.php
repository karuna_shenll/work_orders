<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Admin</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin Theme #2 for " name="description" />
<meta content="" name="author" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/pages/css/login-3.min.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="favicon.ico" />
</head>
<style>
    .custombtn {
        width: 85px;
        border-radius: 6px !important;
        box-shadow: 0px 3px 1px #888888 !important;
    }
    .customlogin, .customlogin:hover {
        background-color: #11527c !important;
        border-color: #11527c !important;
        color: white;
    }
    .alert-success {
        color: #3c763d;
        background-color: #dff0d8;
        border-color: #d6e9c6;
    }
    .login {
        background-image: url(../assets/layouts/layout2/img/bg.png);
        width: 100%;
        height: auto;
    } 
    .business-login {
        border: 1px solid #11527c;
        background-color: #11527c;
        width: 360px;
        color: white;
        position: relative;
        left: -30px;
        top: -20px;
        text-align: center;
        padding-top: 15px;
        padding-bottom: 15px;
        font-size: 20px;
    }
</style>
<body class="login">
    <!-- <img src="../assets/layouts/layout2/img/bg.png" class="img-responsive"> -->
    <div class="col-md-12">
        <div class="col-md-6" style="margin-top: 200px;">
            <div class="logo vendorlogin" >
                <a href="index">
                    <img src="../assets/layouts/layout2/img/logo.png" alt="logo" class="law-pro"/> 
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="content logincontent">
                <div class="business-login"> LOGIN </div>
                <form class="login-form" method="post" id="frm_login" action="dashboard" style="<?php echo $displayshow?>"> 
                    <!-- <h3 style="text-align: center;">Login</h3> -->
                    <?php if (!empty($message)) { ?>
                    <div class="alert alert-<?php echo $alertclass ?>">
                        <button class="close" data-close="alert"></button>
                        <span><?php echo $message ?></span>
                    </div>
                    <?php } ?>
                    <?php if (!empty($loginerror_message)) { ?>
                    <div class="alert alert-<?php echo $alertclass ?>">
                        <button class="close" data-close="alert"></button>
                        <span><?php echo $loginerror_message ?></span>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Email</label>
                        <div class="input-icon">
                            <i class="fa fa-user"></i>
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label visible-ie8 visible-ie9">Password</label>
                        <div class="input-icon">
                            <i class="fa fa-lock"></i>
                            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                    </div>
                    <div class="form-actions">
                        <label class="rememberme mt-checkbox mt-checkbox-outline"></label>
                        <button type="submit" class="btn pull-right customlogin custombtn"><i class="fa fa-sign-in"></i> Login </button>
                    </div>
                    <div class="forget-password">
                        <h4>Forgot your password ?</h4>
                        <p style="margin:5px 0;"> no worries, click <a href="javascript:;" id="forget-password" > here </a> to reset your password. </p>
                    </div>                
                </form>
                <form class="forget-form" action="" id="frm_forgot" method="post" style="<?php echo $displaynone?>">
                    <h3>Forget Password ?</h3>
                    <p> Enter your e-mail address below to reset your password. </p>
                    <?php if (!empty($message2)) { ?>
                    <div class="alert alert-<?php echo $alertclass ?>">
                        <button class="close" data-close="alert"></button>
                        <span><?php echo $message2 ?></span>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <div class="input-icon">
                            <i class="fa fa-envelope"></i>
                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="forgetemail" />
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" id="back-btn" class="btn dark custombtn"><i class="fa fa-arrow-left"></i> Back </button>
                        <button type="submit" class="btn pull-right customlogin custombtn" name="Sendemail" value="ForgotMail"><i class="fa fa-arrow-right"></i> Submit </button>
                    </div>
                </form>
            </div>
        </div>
    </div>    
    <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
    <script src="../assets/pages/scripts/login4.js" type="text/javascript"></script>
</body>
</html>
<script>
    $(document).ready(function(){
        $("#back-btn").click(function(){
        var l = window.location;
        var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1]+"/" +l.pathname.split('/')[2];
        window.location.href=base_url+"/index";
        });
    });
</script>
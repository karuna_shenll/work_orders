<?php
include("header.php");
?>
<form name="dashboard_form" id="dashboard_form" method="post" action="">
    <div class="page-content" id="customer-list-page-content">
        <div class="row food-orders">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light customlistminheight">
                    <div class="portlet-title dashoboardtitle">
                        <div class="dashoboardcaption font-dark caption-new">
                            <span class="caption-subject bold">Work Orders</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                     <div class="portlet-body">
                        <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="shop_details">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details shop">
                                    <div class="number dashboardtext">
                                        <span data-counter="counterup">Enter Shop Details</span>
                                    </div>
                                    <div class="desc"> </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="search_work_order">
                                <div class="visual">
                                    <i class="fa fa-bar-chart-o"></i>
                                </div>
                                <div class="details workorder">
                                    <div class="number dashboardtext">
                                        <span data-counter="counterup">Search Work Order</span>
                                    </div>
                                    <div class="desc"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" data-toggle="modal" href="#modal_reprint" data-backdrop="static">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details reprint">
                                    <div class="number dashboardtext" >
                                        <span data-counter="counterup">Reprint Order</span>
                                    </div>
                                    <div class="desc"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="create_workorder">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details createorder">
                                    <div class="number dashboardtext">
                                        <span data-counter="counterup" >Create New Work Order</span>
                                    </div>
                                    <div class="desc"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</form>
<?php 
include_once("footer.php"); 
?>

<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
// include_once('../api/sms.php');
// include_once('../api/Twilio/sms.php');
$commonAppApi = new Common($dbconn);
$Qry = "SELECT * FROM tbl_users WHERE type = 'labour' AND status = 1 AND status !='2' ORDER BY user_id DESC";
$labourInfo = $commonAppApi->funBckendExeSelectQuery($Qry,$qryParams);


$mode = "Add";
if (isset($_GET['id']) && ($_GET['id']) != "") {

    $selQryParams = array (":job_id" =>$commonAppApi->decode($_GET['id']));
    $whereCondtn = $commonAppApi->funParseQryParams($selQryParams, "job_id", "AND"); 
    $reqQryParams = array ("fetchType" => "singlerow", "selectField" => "", "tableName" => "tbl_jobs", "whereCondition" => $whereCondtn);
    $getContactRes = $commonAppApi->funExeSelectQuery($reqQryParams, $selQryParams);
    $job_id = $getContactRes["job_id"];
    $labour_id = $getContactRes["user_id"];
    $job_date = date("m/d/Y", strtotime($getContactRes["job_date"]));
    $job_time = $getContactRes["job_time"];

    $mode = "Edit";
}

if (isset($_POST['labour_id']) && ($_POST['labour_id']) != "" && isset($_POST['job_date']) && $_POST['job_date']!="" && isset($_POST['job_time']) && $_POST['job_time']!="") {

    if ($mode == "Add") {
        $insQryParams = array (":user_id" => $_POST["labour_id"],
                            ":job_date" => date("Y-m-d", strtotime($_POST["job_date"])),
                            ":job_time" => $_POST["job_time"],
                        );
        $insQryResponse = $commonAppApi->funExeInsertRecord("tbl_jobs", $insQryParams);

        if (!empty($insQryResponse)) {

            $labourSelQryParams = array (":user_id" =>trim($_POST['labour_id']), ":status" => 1);
            $whereCondtnLabour = $commonAppApi->funParseQryParams($labourSelQryParams, "status", "AND"); 
            $reqLabourQryParams = array (
                                        "fetchType" => "singlerow",
                                        "selectField" => "",
                                        "tableName" => "tbl_users",
                                        "whereCondition" => $whereCondtnLabour);
            $getLabourRes = $commonAppApi->funExeSelectQuery($reqLabourQryParams, $labourSelQryParams);
            $name = $getLabourRes["first"]."  ".$getLabourRes["last"];
            $mailParams = array(
                        "fromAddress" => "test@test.com",
                        "toAddress" => $getLabourRes["email"],
                        "contactName" => "THIYAGARAJAN",
                        "subject" => "Good job created",
                        "bodyMsg" => "<p>Dear ".$name.",</p>
                                    <p>I have job created</p>
                                    <p>Job Date: ".date("Y-m-d", strtotime($_POST["job_date"]))."</p>
                                    <p>Job Timing: ".$_POST["job_time"]."</p>
                                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                    <p style='line-height:5px;'>Wishing you the very best,</p> 
                                    <p style='line-height:0px;'>Business team</p>"
                        );
            // $smsParams = array('phoneNumber' => $getLabourRes["phone"], 'name' => $name, 'jobDate' => date("Y-m-d", strtotime($_POST["job_date"])), 'jobTiming' => $_POST["job_time"]);
            $sendEmail = $commonAppApi->sendEmailNotification($mailParams);
            // $smsEmail = sendSmsNotification($smsParams);

            header("Location:jobs?msg=".$commonAppApi->encode('1'));
            exit;
        } else {
            header("Location:job?msg=".$commonAppApi->encode('2'));
            exit;
        }
    } else {
        $qryParams = array (":user_id" => $_POST["labour_id"],
                            ":job_date" => date("Y-m-d",strtotime($_POST["job_date"])),
                            ":job_time" => $_POST["job_time"],
                            ":job_id" => $job_id
                            );
        $setCondtn = $commonAppApi->funParseQryParams($qryParams, "job_id", ",");
        $reqQryParams = array ("tableName" => "tbl_jobs", "setCondtn" => $setCondtn, "whereCondition" => "job_id = :job_id" );
        $updateQryResponse = $commonAppApi->funExeUpdateRecord($reqQryParams, $qryParams);
        header("Location:jobs?msg=".$commonAppApi->encode('2'));
        exit;
    }
} 

/*End of paging*/
include("header.php");

?>
<form name="job_form" id="job_form" method="post" action="">
    <div class="page-content" id="contact-list-page-content">
        <div class="row food-orders">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="portlet light customlistminheight">
                    <div class="portlet-title" >
                        <div class="caption font-dark caption-new">
                            <img src="../assets/layouts/layout2/img/jobs-25x25-1.png" class="icon-img">
                            <span class="caption-subject bold uppercase icon-title-name"><?php echo $mode ?> Job</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <?php
                        if ($commonAppApi->decode($_GET['msg']) == 2) { ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <p>You have add job error!!!</p>
                        </div>
                    <?php } ?>
                     <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 reportcontactsearch" id="contactListresponsive">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label><strong>Name:</strong></label>
                                        <select name="labour_id" id="labour_id" class="form-control" <?php echo $disabled; ?>>
                                            <option value="">Select labour</option>
                                            <?php
                                            foreach ($labourInfo as $getcoachData) {
                                                echo "<option value=".$getcoachData["user_id"].">". $getcoachData["first"]."</option>";
                                            }
                                            ?>
                                        </select>
                                        <script>$("#labour_id").val("<?php echo $labour_id; ?>")</script>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label><strong>Job Date:</strong></label>
                                        <input class="form-control input-medium date-picker" size="16" type="text" name="job_date" id="job_date" value="<?php echo $job_date ?>" />
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <label><strong>Job Time:</strong></label>
                                        <input type="text" name="job_time" id="job_time" class="form-control timepicker timepicker-default" value="<?php echo $job_time ?>">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 contact-add-padding">
                                    <div class="col-md-6 col-sm-6 col-xs-6 contact-save">
                                        <button type="submit" class="btn yellow custombtn"><i class="fa fa-floppy-o"></i> <?php echo $mode ?></button>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <a href="../admin/jobs">
                                        <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Cancel</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include_once("footer.php"); ?>
<script src="../assets/layouts/layout2/scripts/custom.js" type="text/javascript"></script>
<?php
include("header.php");
?>
<form name="active_list_form" id="active_list_form" method="post" action="">
    <div class="page-content" id="contact-list-page-content">
    	<div class="row food-orders">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="portlet light customlistminheight">
                    <div class="portlet-title" >
                        <div class="caption font-dark caption-new">
                            <img src="../assets/layouts/layout2/img/work-order_active.png" class="icon-img">
                            <span class="caption-subject bold uppercase icon-title-name">Search Work Orders</span>
                        </div>
                        <div class="tools"></div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 reportcontactsearch" id="contactListresponsive">
                                <div class="col-md-12 col-sm-12 col-xs-12 remove-left-right-padding">
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Work Order #:</label>
                                         <input type="text" name="search_name" id="search_name" class="form-control" placeholder="Work Order #" value="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Date Created:</label>
                                         <input type="text" name="search_name" id="search_name" class="form-control date-picker" placeholder="mm/dd/yyyy" value="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Item #:</label>
                                         <input type="text" name="search_name" id="search_name" class="form-control" placeholder="Item #" value="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Date Completed:</label>
                                        <input type="text" name="search_email" id="search_email" class="form-control date-picker" placeholder="mm/dd/yyyy" value="">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <label>Status:</label>
                                        <select  name="search_status" id="search_status" class="form-control">
                                            <option value="">Select</option>
                                            <option value="1">Pending</option>
                                            <option value="0">Completed</option>
                                        </select>
                                        <script>$("#search_status").val("<?php echo $search_status;?>")</script>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12 remove-left-right-padding">
                                        <div class="col-md-12 col-sm-12 col-xs-12 search-orderlist-btns remove-left-right-padding">
                                            <div class="col-md-6 col-sm-6 col-xs-6 remove-left-right-padding"><button type="button" class="btn yellow custombtn" id="Search"><i class="fa fa-search"></i> Search</button></div>
                                            <div class="col-md-6 col-sm-6 col-xs-6 remove-left-right-padding"><button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Reset</button></div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <h1></h1>
                    <div class="col-md-12 col-sm-12 col-xs-12 search-orderlist-btns remove-left-right-padding">
                        <div class="col-md-6 col-sm-6 col-xs-6 remove-left-right-padding"><button type="button" class="btn yellow custombtn" id="Search"><i class="fa fa-file-pdf-o"></i> export</button></div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="table-view-back-color-design">
                                        <th title="FIRST NAME">Work Order #</th>
                                        <th title="LAST NAME">Date Created</th>
                                        <th title="EMAIL">Item #</th>
                                        <th title="PHONE NUMBER">Date Completed</th>
                                        <th title="STATUS">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>05/12/2017</td>
                                        <td>12</td>
                                        <td>05/18/2018</td>
                                        <td>Completed</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>05/12/2017</td>
                                        <td>12</td>
                                        <td>06/26/2018</td>
                                        <td>Completed</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>05/12/2017</td>
                                        <td>35</td>
                                        <td>05/21/2018</td>
                                        <td>Completed</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>05/12/2017</td>
                                        <td>18</td>
                                        <td>05/10/2018</td>
                                        <td>Completed</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include_once("footer.php"); ?>
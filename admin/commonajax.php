<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');
error_reporting(E_ALL);
$commonAppApi = new Common($dbconn);


if(isset($_POST["UserTaskId"]) && $_POST["UserTaskId"] != "" && isset($_POST["CommentTask"]) && $_POST["CommentTask"] != "") {
	$UserTaskId = $_POST["UserTaskId"];
	if ($UserTaskId) {
		$selQryParams = array (":usertask_id" =>$UserTaskId);
	    $whereCondtn = $commonAppApi->funParseQryParams($selQryParams, "usertask_id", "AND"); 
	    $reqQryParams = array (
	                        "fetchType" => "singlerow",
	                        "selectField" => "",
	                        "tableName" => "pro_usertasks",
	                        "whereCondition" => $whereCondtn
	                    );
	    $getContactRes = $commonAppApi->funExeSelectQuery($reqQryParams, $selQryParams);
	    echo json_encode($getContactRes);
	}
}
elseif(isset($_POST["usertaskId"]) && $_POST["usertaskId"] != "" && isset($_POST["comments"]) && $_POST["comments"] != "" && isset($_POST["status"]) && $_POST["status"] != "") {
	$usertaskId = $_POST["usertaskId"];
	$comments = $_POST["comments"];
	$status = ($_POST["status"]=="1")?'<img src="../assets/layouts/layout2/img/ok-btn.png">':'<img src="../assets/layouts/layout2/img/close-btn.png">';
	if (!empty($usertaskId) && !empty($status) && !empty($comments) ) {
	    $qryParams = array ( 
                                ":usertask_id" => $usertaskId,
                                ":comment" => $comments,
                                ":complete" => $_POST["status"]
                            );
        $setCondtn = $commonAppApi->funParseQryParams($qryParams, "complete", ",");
        $reqQryParams = array (
                            "tableName" => "pro_usertasks",
                            "setCondtn" =>$setCondtn,
                            "whereCondition" => "usertask_id=:usertask_id" 
                        );
        $updateQryResponse = $commonAppApi->funExeUpdateRecord($reqQryParams, $qryParams);
        $getContactRes=array("message"=>"success","comment"=>$_POST["comments"],"status"=>$status);
	    echo json_encode($getContactRes);
	    exit;
	} else {
		$getContactRes=array("message"=>"Comments not updated");
	    echo json_encode($getContactRes);
	    exit;
	}
} 

elseif(isset($_POST["currentpass"]) && $_POST["currentpass"] != "" && isset($_POST["userid"]) && $_POST["userid"] != "") {

	$currentpass = $commonAppApi->encode($_POST["currentpass"]);
	$userid = $_POST["userid"];

	$selQryParams = array (	
				        ":password" => $currentpass,
					     ":user_id" => $userid
					);
	$whereCondtn = $commonAppApi->funParseQryParams($selQryParams, "user_id", "AND");
	$reqQryParams = array (
								"fetchType" => "singleRow",
								"selectField" => "",
								"tableName" => "pro_users",
								"whereCondition" => $whereCondtn
							);		    
	$selQryResponse = $commonAppApi->funExeSelectQuery($reqQryParams, $selQryParams);	
	if(count($selQryResponse)>0) {
			echo "exists";
		  exit;
		} else {
			 echo "not-exists";
		  exit;
		}
}

?>
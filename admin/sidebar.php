<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">

        <?php 
        error_reporting(0);
            $navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
        ?>
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start <?php echo ($navigationURL == 'dashboard' || $navigationURL == 'dashboard')?'active':''; ?> open side-menu-logo">
                <a href="dashboard" class="side-menu nav-link nav-toggle home-image">
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start <?php echo ($navigationURL == 'create_workorder' || $navigationURL == 'create_workorder')?'active':''; ?> open side-menu-logo">
                <a href="create_workorder" class="side-menu nav-link nav-toggle neworder-image">
                    <span class="title">Create Work Order</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item start open <?php echo ($navigationURL == 'shop_details' || $navigationURL == 'shop_details')?'active':''; ?> side-menu-logo">
                <a href="shop_details" class="nav-link nav-toggle shop-image">
                    <span class="title">Shop Details</span>
                    <span class="selected"></span>
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL == 'search_work_order' || $navigationURL == 'search_work_order')?'active':''; ?> side-menu-logo">
                <a href="search_work_order" class="nav-link nav-toggle search-image">
                    <span class="title">Search Work Order</span>
                    <span class="selected"></span>
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL == 'print_order' || $navigationURL == 'reprint_work_order')?'active':''; ?> side-menu-logo">
                <a class="nav-link nav-toggle print-image" data-toggle="modal" href="#modal_reprint" data-backdrop="static">
                    <span class="title">Reprint Order</span>
                    <span class="selected"></span>
                </a>                            
            </li>
            <li class="nav-item start open <?php echo ($navigationURL == 'manage_users'|| $navigationURL == 'users_list')?'active':''; ?> side-menu-logo">
                <a href="users_list" class="nav-link nav-toggle dashboard-image" >
                    <span class="title">Manage users</span>
                    <span class="selected"></span>
                </a>                            
            </li>
        </ul>
    </div>
</div>
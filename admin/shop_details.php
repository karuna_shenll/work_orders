<?php
include("header.php");
?>
<form name="active_list_form" id="active_list_form" method="post" action="">
    <div class="page-content" id="contact-list-page-content">
    	<div class="row food-orders">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="portlet light customlistminheight">
                    <div class="portlet-title" >
                        <div class="caption font-dark caption-new">
                            <img src="../assets/layouts/layout2/img/shop_active.png" class="icon-img">
                            <span class="caption-subject bold uppercase icon-title-name">Shop Details</span>
                        </div>
                        <div class="tools"></div>
                    </div>

                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr class="table-view-back-color-design">
                                        <th title="FIRST NAME">Work Order #</th>
                                        <th title="LAST NAME">Date Created</th>
                                        <th title="EMAIL">Item #</th>
                                        <th title="PHONE NUMBER">Date Completed</th>
                                        <th title="STATUS">Status</th>
                                        <th class="text-center" title="Action">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>05/12/2017</td>
                                        <td>6</td>
                                        <td>05/18/2018</td>
                                        <td>Completed</td>
                                        <td class="lasttd" style="text-align: -webkit-center;">
                                            <table>
                                                <tr>
                                                    <td>
                                                       <a href="print_work_order" data-toggle="tooltip"
                                                           title='Edit' class="color-tooltip">
                                                           <button type="button" class="btn btn-info " style="border-radius: 6px !important;">
                                                               <!-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> -->
                                                               Enter Shop Details
                                                           </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>   
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>05/12/2017</td>
                                        <td>8</td>
                                        <td>07/23/2018</td>
                                        <td>Pending</td>
                                        <td class="lasttd" style="text-align: -webkit-center;">
                                            <table>
                                                <tr>
                                                    <td>
                                                       <a href="print_work_order" data-toggle="tooltip"
                                                           title='Edit' class="color-tooltip">
                                                           <button type="button" class="btn btn-info " style="border-radius: 6px !important;">
                                                               <!-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> -->
                                                               Enter Shop Details
                                                           </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>   
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>05/12/2017</td>
                                        <td>2</td>
                                        <td>08/12/2018</td>
                                        <td>Completed</td>
                                        <td class="lasttd" style="text-align: -webkit-center;">
                                            <table>
                                                <tr>
                                                    <td>
                                                       <a href="print_work_order" data-toggle="tooltip"
                                                           title='Edit' class="color-tooltip">
                                                           <button type="button" class="btn btn-info " style="border-radius: 6px !important;">
                                                               <!-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> -->
                                                               Enter Shop Details
                                                           </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>   
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>05/12/2017</td>
                                        <td>2</td>
                                        <td>05/30/2018</td>
                                        <td>Pending</td>
                                        <td class="lasttd" style="text-align: -webkit-center;">
                                            <table>
                                                <tr>
                                                    <td>
                                                       <a href="print_work_order" data-toggle="tooltip"
                                                           title='Edit' class="color-tooltip">
                                                           <button type="button" class="btn btn-info "  style="border-radius: 6px !important;">
                                                               <!-- <i class="fa fa-pencil-square-o" aria-hidden="true"></i> -->
                                                               Enter Shop Details
                                                           </button>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>   
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include_once("footer.php"); ?>
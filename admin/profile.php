<?php
include_once('../includes/configure.php');
include_once('../includes/session_check.php');
include_once('../api/Common.php');
error_reporting(0);
$commonAppApi = new Common($dbconn);
$AlertMessage = '';
$AlertClass = '';
$AlertFlag = false;
if (isset($_GET["msg"])) {
	if ($commonAppApi->decode($_GET["msg"]) == 1) {
		$AlertMessage = "Profile updated successfully.";
        $AlertClass = "alertsuccess";
        $AlertFlag = true;
	}
	if ($commonAppApi->decode($_GET["msg"]) == 2) {
		$AlertMessage = "Profile image updated successfully.";
        $AlertClass = "alertsuccess";
        $AlertFlag = true;
	}
	if ($commonAppApi->decode($_GET["msg"]) == 3) {
		$AlertMessage = "Password updated successfully.";
        $AlertClass = "alertsuccess";
        $AlertFlag = true;
	}
	if ($commonAppApi->decode($_GET["msg"]) == 4){
		$AlertMessage = "Sorry, there was an error uploading your file.";
        $AlertClass = "alert-danger";
        $AlertFlag = true;
	}
	if ($commonAppApi->decode($_GET["msg"]) == 5){
		$AlertMessage = "Sorry, your file was not uploaded.";
        $AlertClass = "alert-danger";
        $AlertFlag = true;
	}
	if ($commonAppApi->decode($_GET["msg"]) == 6){
		$AlertMessage = "Sorry, your file is too large.";
        $AlertClass = "alert-danger";
        $AlertFlag = true;
	}
}
$activeStateQryParams = array (":active" =>1);
$whereStateCondtnActive = $commonAppApi->funParseQryParams($activeStateQryParams, "active", "AND");
$stateReqQryParams = array (
    "selectField" => "*",
    "tableName" => "pro_states",
    "orderByCondtn" => "state_id",
    "whereCondition" => $whereStateCondtnActive,
    "fetchType" => "multiple"
);
$stateInfo = $commonAppApi->funExeSelectQuery ($stateReqQryParams, $activeStateQryParams);
$activeCompanyQryParams = array (":active" =>1);
$whereCondtnActive = $commonAppApi->funParseQryParams($activeCompanyQryParams, "active", "AND");
$companyReqQryParams = array (
    "selectField" => "",
    "tableName" => "pro_companies",
    "fetchType" => "",
    "whereCondition" => $whereCondtnActive
);
$companyInfo = $commonAppApi->funExeSelectQuery ($companyReqQryParams, $activeCompanyQryParams);
$selQryParams = array (	
				":user_id" => $_SESSION['ADMIN_ID']
				);
$whereCondtn = $commonAppApi->funParseQryParams($selQryParams, "user_id", "AND");
$reqQryParams = array (
					"fetchType" => "singleRow",
					"selectField" => "",
					"tableName" => "pro_users",
					"whereCondition" => $whereCondtn
				);		    
$selQryResponse = $commonAppApi->funExeSelectQuery($reqQryParams, $selQryParams);

if ($_POST["changeproflie"] == "proflie" && !empty($_POST["changeproflie"]) && !empty($_POST["first_name"])) {
	$qryParams = array ( 
                        ":first_name" =>$_POST["first_name"],
                        ":last_name" =>$_POST["last_name"],
                        ":company_id" =>$_POST["firmname"],
                        ":email" =>$_POST["email"],
                        ":telephone" =>$_POST["phone"],
                        ":address" =>$_POST["address"],
                        ":city" =>$_POST["city"],
                        ":state_code" =>$_POST["state"],
                        ":user_id" => $_SESSION['ADMIN_ID']);
    $setCondtn = $commonAppApi->funParseQryParams($qryParams, "user_id", ",");
    $reqQryParams = array ("tableName" => "pro_users", "setCondtn" =>$setCondtn, "whereCondition" => "user_id=:user_id");
    $updateQryResponse = $commonAppApi->funExeUpdateRecord($reqQryParams, $qryParams);
    header("Location:profile?msg=".$commonAppApi->encode('1'));
    exit;
}	

if ($_POST["changepass"] == "changepassword" && !empty($_POST["changepass"]) && !empty($_POST["password"])) {
	$password = $_POST["password"];
	$changepassword =$commonAppApi->encode($password);
	$qryParams = array (":password" => $changepassword, ":user_id" => $_SESSION['ADMIN_ID']);
    $setCondtn = $commonAppApi->funParseQryParams($qryParams, "user_id", ",");
    $reqQryParams = array ("tableName" => "pro_users", "setCondtn" =>$setCondtn, "whereCondition" => "user_id=:user_id");
    $updateQryResponse = $commonAppApi->funExeUpdateRecord($reqQryParams, $qryParams);
    header("Location:profile?msg=".$commonAppApi->encode('3'));
    exit;
}

if ($_POST["uploadlogo"]=="uploadlogo" && !empty($_POST["uploadlogo"])) {
	$target_dir = "../uploads/user_profile/";
    $target_file = $target_dir . basename($_FILES["file"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    $AlertMessage = "";
    if($check !== false) {
        $uploadOk = 1;
        // Check file size
        if ($_FILES["file"]["size"] < 500000) {
            $uploadOk = 1;
            $uploadOk = 1;
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 1) {
                // if everything is ok, try to upload file
                if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                } else {
                	$AlertNumber = 4;
                }
            } else {
            	$AlertNumber = 5;
            }
        } else {
            $AlertNumber = 6;
        }
    }  
    $uploadfile=($_FILES["file"]["name"]=="")?$_POST["hndimage"]:$_FILES["file"]["name"];
    if(empty($AlertNumber)){   
	    $qryParam = array (":avatar" => $uploadfile, ":user_id" => $_SESSION['ADMIN_ID']);
	    $setCondtn = $commonAppApi->funParseQryParams($qryParam, "user_id", ",");
	    $reqQryParam = array ("tableName" => "pro_users", "setCondtn" =>$setCondtn, "whereCondition" => "user_id=:user_id");
	    $updateQryResponse = $commonAppApi->funExeUpdateRecord($reqQryParam, $qryParam);
	    header("Location:profile?msg=".$commonAppApi->encode('2'));
	    exit;
    } else {
    	header("Location:profile?msg=".$commonAppApi->encode($AlertNumber));
	    exit;
    }
}
include("header.php");
?>
<link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<div class="page-content">
	<div class="row">
        <div class="col-md-12">
        	<?php if ($AlertFlag == true) { ?>
	            <div class="alert alert-block fade in <?php echo $AlertClass; ?>">
	                <button type="button" class="close" data-dismiss="alert"></button>
	                <p> <?php echo $AlertMessage; ?> </p>
	            </div>
            <?php } ?>
        	<div class="">
	            <div class="profile-sidebar">
	                <div class="portlet light profile-sidebar-portlet ">
	                    <div class="profile-userpic">
	                    	<?php
                            $ImgURL = ($selQryResponse["avatar"]=="")?"no_image.png":$selQryResponse["avatar"];
	                    	?>
	                        <img src="../uploads/user_profile/<?php echo $ImgURL;  ?>" class="img-responsive" alt="" style="width:150px;height:150px;"> </div>
	                    <div class="profile-usertitle" style="padding-bottom: 20px;">
	                        <div class="profile-usertitle-name bold uppercase"><?php echo $selQryResponse["first_name"]."&nbsp;".$selQryResponse["last_name"]; ?></div>
	                        <div class="profile-usertitle-job bold uppercase"> <?php echo $_SESSION['ADMIN_TYPE']?></div>
	                    </div>
	                </div>
	            </div>
        	</div>
            <!-- BEGIN PROFILE SIDEBAR -->
    		<div class="profile-content">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line" id="profiletabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase profile-head"> <i class="icon-user"></i> Profile</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">INFO</a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-toggle="tab">AVATAR</a>
                                </li>
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab">PASSWORD</a>
                                </li>
                               
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content"><!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_1_1">
                                    <form role="form" name="form_proflie" action="" method="post">
                                        <div class="form-group">
                                            <label class="control-label">First Name</label>
                                            <input type="text" name="first_name" id="first_name" placeholder="First Name" class="form-control" value="<?php echo $selQryResponse["first_name"] ?>"/> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" name="last_name" id="last_name" placeholder="Last Name" class="form-control" value="<?php echo $selQryResponse["last_name"]?>"/> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            <input type="text" name="email" id="email" placeholder="Email Address" class="form-control" value="<?php echo $selQryResponse["email"]?>"/> 
                                        </div>
                                        <?php if ($_SESSION['ADMIN_TYPE'] != "admin") { ?>
                                        <div class="form-group">
                                            <label class="control-label">Firm Name:</label>
		                                    <select  name="firmname" id="firmname" class="form-control">
		                                        <option value="">Select firm Name</option>
		                                        <?php 
		                                            foreach ($companyInfo as $key => $value) { ?>
		                                               <option value="<?php echo $value["company_id"]; ?>"><?php echo $value["company_name"]; ?></option>
		                                            <?php } ?>
		                                    </select>
		                                    <script>$("#firmname").val("<?php  echo $selQryResponse["company_id"];?>")</script>
		                                </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <label class="control-label">Mobile Number</label>
                                            <input type="text" name="phone" id="phone" placeholder="Mobile Number" class="form-control" value="<?php echo $selQryResponse["telephone"]?>"/> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Address</label>
                                            <textarea name="address" id="address" class="form-control" rows="3" placeholder="Address"><?php echo $selQryResponse["address"]?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">City</label>
                                            <input type="text" name="city" id="city" placeholder="City" class="form-control" value="<?php echo $selQryResponse["city"]?>"/> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">State</label>
                                            <select  name="state" id="state" class="form-control">
		                                        <option value="">Select State</option>
		                                        <?php 
		                                            foreach ($stateInfo as $key => $value) { ?>
		                                               <option value="<?php echo $value["state_code"]; ?>"><?php echo $value["state_name"]; ?></option>
		                                            <?php } ?>
		                                    </select>
		                                    <script>$("#state").val("<?php echo $selQryResponse["state_code"];?>")</script>
                                        </div>
                                       <div class="margiv-top-10">
                                            <button type="submit" name="changeproflie" class="btn yellow custombtn" value="proflie"> <i class="fa fa-floppy-o"></i> Update </button>
                                            <?php if ($_SESSION['ADMIN_TYPE'] == "admin") { ?>
                                                <a href="../admin/dashboard">
                                                    <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Cancel</button>
                                                </a>
                                            <?php } else { ?>
                                                 <a href="../admin/clients">
                                                    <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Cancel</button>
                                                </a>
                                            <?php }?>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                                <!-- CHANGE AVATAR TAB -->
                                <div class="tab-pane" id="tab_1_2">
                                   
                                    <form name="formupload" action="" role="form" method="post" enctype="multipart/form-data">
                                    	<input type="hidden" name="hndimage" value="<?php echo $selQryResponse["avatar"]?>">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                            	<?php
                                            	 $imgURL=($selQryResponse["avatar"]=="")?"../uploads/user_profile/no-image.png":"../uploads/user_profile/".$selQryResponse["avatar"];
                                            	?>
                                            	<div class=" thumbnail" style="width: 200px; height: 150px;">
                                            		<img width="200" height="150" class="imgpreview" style="font-size: 12px;color: black;font-weight: bold;" src="<?php echo  $imgURL?>" alt="Preview Image" border="0">
                                           
                                             	</div>
                                                <div>
                                                    <input type="file" name="file" id="file" class="" placeholder="Avatar" value="" style="margin-bottom: 10px;" accept="image/jpg,image/png,image/jpeg,image/gif" onchange="showimagepreview(this,this.id)">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="margin-top-10">
                                            <button type="submit" name="uploadlogo" class="btn yellow custombtn" value="uploadlogo"> <i class="fa fa-floppy-o"></i> Update </button>
                                            <a href="../admin/profile">
                                                <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Cancel</button>
                                            </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END CHANGE AVATAR TAB -->
                                <!-- CHANGE PASSWORD TAB -->
                                <div class="tab-pane" id="tab_1_3">
                                    <form id="form_changepassword" name="form_changepassword" action="" method="POST">
                                    	<input type="hidden" name="hnduserid" id="hnduserid" value="<?php echo $_SESSION['ADMIN_ID']?>">
                                        <div class="form-group">
                                            <label class="control-label">Current Password</label>
                                            <input type="password" name="current_pass" id="current password" class="form-control" placeholder="Current Password" /> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">New Password</label>
                                            <input type="password" name="password" id="password" class="form-control" placeholder="New Password"/> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Re-type New Password</label>
                                            <input type="password" name="conpassword" class="form-control" placeholder="Re-type New Password" /> </div>
                                        <div class="margin-top-10">
                                            <button type="submit" name="changepass" class="btn yellow custombtn" value="changepassword"> <i class="fa fa-floppy-o"></i> Update </button>
                                            <a href="../admin/profile">
                                                <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Cancel</button>
                                            </a>
                                        </div>
                                    </form>
                                </div><!-- END CHANGE PASSWORD TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- END BEGIN PROFILE SIDEBAR -->
        </div>
    </div>
</div>
<?php
include("footer.php");
?>
<script src="../assets/layouts/layout2/scripts/profile.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script><script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
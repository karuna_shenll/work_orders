<?php
include("header.php");
?>
<form name="active_list_form" id="active_list_form" method="post" action="">
    <div class="page-content" id="contact-list-page-content">
    	<div class="row food-orders">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="portlet light customlistminheight">
                    <div class="portlet-title" >
                        <div class="caption font-dark caption-new">
                            <img src="../assets/layouts/layout2/img/labours-25x25-1.png" class="icon-img">
                            <span class="caption-subject bold uppercase icon-title-name">Users Lists</span>
                        </div>
                        <div class="tools">
                            <a href="add_users" class="btn custombtncolor custombtn dropdown-toggle add_charity" style="height: 36px;width:100px;color:white"><i class="fa fa-plus-circle" aria-hidden="true"></i>
                            Add User&nbsp;
                        </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                   <tr class="table-view-back-color-design">
                                        <th title="FIRST NAME">S.No #</th>
                                        <th title="LAST NAME">First Name</th>
                                        <th title="EMAIL">Last Name</th>
                                        <th title="PHONE NUMBER">Email</th>
                                        <th title="STATUS">User Type</th>
                                        <th title="STATUS">Status</th>
                                        <th class="text-center" title="Action">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Avalon </td>
                                        <td>Roberts</td>
                                        <td>avalon@gmail.com</td>
                                        <td>Admin</td>
                                        <td>Active</td>
                                        <td>
                                            <a href="manage_users" class="btn btn-icon-only green">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:;" class="btn btn-icon-only red">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Rose</td>
                                        <td>Dylan</td>
                                        <td>dylan@yahoo.com</td>
                                        <td>User</td>
                                        <td>Active</td>
                                        <td>
                                           <a href="manage_users" class="btn btn-icon-only green">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:;" class="btn btn-icon-only red">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Ostara </td>
                                        <td>Faith</td>
                                        <td>faith@gmail.com</td>
                                        <td>Super-Admin</td>
                                        <td>Inactive</td>
                                        <td>
                                            <a href="manage_users" class="btn btn-icon-only green">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:;" class="btn btn-icon-only red">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Santa </td>
                                        <td>Catalina</td>
                                        <td>catalina@gmail.com</td>
                                        <td>Admin</td>
                                        <td>Active</td>
                                        <td>
                                            <a href="manage_users" class="btn btn-icon-only green">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:;" class="btn btn-icon-only red">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include_once("footer.php"); ?>
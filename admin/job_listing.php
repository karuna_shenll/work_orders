<?php
include_once('../includes/configure.php');
include_once('../api/Common.php');
include_once('../includes/session_check.php');

$Page = 1; $RecordsPerPage = 25;
$TotalPages = 0;
$commonAppApi = new Common($dbconn);
if (isset($_POST["jobDetJsonString"])) {
    $jobsSearch = json_decode($_POST["jobDetJsonString"], true);
    $jobuser = trim(!empty($jobsSearch["searchName"])) ? trim($jobsSearch["searchName"]) : "" ; 
    $status = $jobsSearch["status"];
    if (isset($jobsSearch['HdnPage']) && is_numeric($jobsSearch['HdnPage']))
        $Page = $jobsSearch['HdnPage'];
}  
?>
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>"> 
<div class="portlet-body">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="tbl_coach_list">
            <thead>
                <tr class="table-view-back-color-design">
                    <th>#</th>
                    <th>Assigned to</th>
                    <th>Job date</th>
                    <th>Time</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $qryParams = array();
                $QryCondition = "";
                if (!empty($contactname)) {
                    $QryCondition.=" AND (users.first like :first OR users.last like :last)";
                    $qryParams[":first"] = "%".$contactname."%";
                    $qryParams[":last"] = "%".$contactname."%";
                }
                // if (is_numeric($status)) {
                //     $QryCondition.=" AND users.status=:status";
                //     $qryParams[":status"] = $status;
                // }

                $Qry = "SELECT * FROM tbl_jobs as jobs join tbl_users as users on jobs.user_id = users.user_id WHERE users.type = :type AND jobs.status !=2  ".$QryCondition."  order by jobs.user_id desc";
                $qryParams[":type"] = "labour";
                
                $getResCnt = $commonAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                if (count($getResCnt,COUNT_RECURSIVE) > 1) {
                    $TotalPages = ceil(count($getResCnt) / $RecordsPerPage);
                    $Start = ($Page-1)*$RecordsPerPage;
                    $sno = $Start+1;
                    $Qry.=" limit $Start,$RecordsPerPage";
                    $getcontact = $commonAppApi->funBckendExeSelectQuery($Qry,$qryParams);
                    
                    if (count($getcontact)>0) {
                        foreach ($getcontact as $jobListData) {

                            $statusimg = "";
                            if ($jobListData["status"] == "1") {
                                $statusimg = "../assets/layouts/layout2/img/ok-btn.png";
                            } else {
                                $statusimg = "../assets/layouts/layout2/img/close-btn.png";
                            }
                 ?>
                <tr>
                    <td><?php echo $sno;?></td>
                    <td><?php echo $jobListData["first"];?></td>
                    <td><?php echo $jobListData["job_date"];?></td>
                    <td><?php echo $jobListData["job_time"];?></td>
                    <!-- <td><img src="<?php echo $statusimg; ?>"></td> -->
                    <td class="lasttd" style="text-align: -webkit-center;">
                        <table>
                            <tr>
                                <td>
                                   <a href="../admin/job?id=<?php echo $commonAppApi->encode($jobListData["job_id"]); ?>" data-toggle="tooltip"
                                       title='Edit' class="color-tooltip">
                                       <button type="button" class="btn btn-info task-edit-comment" >
                                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                       </button>
                                    </a>
                                </td>
                                <td>
                                   <a href="../admin/jobs?id=<?php echo $commonAppApi->encode($jobListData["job_id"]); ?>" onclick="return confirm('Are you sure want to delete this job?')" data-toggle="tooltip" 
                                       title='Delete' class="color-tooltip">
                                      <button type="button" class="btn btn-info task-edit-comment">
                                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                                       </button>
                                    </a>
                                </td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                <?php $sno++;     
                        }
                    } else {
                        echo "<tr><td colspan='5'>No client(s) found.</td></tr>";
                    }   
                } else {
                    echo "<tr><td colspan='5'>No client(s) found.</td></tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
<?php
    if ($TotalPages > 1) {
        echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
        $FormName = "clientList_form";
        require_once ("paging.php");
        echo "</td></tr>";
    }
?>
</div>
<!-- <script src="../assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script> -->
<script>
    $(document).ready(function(){
       $('[data-toggle="tooltip"]').tooltip();   
    });
</script>
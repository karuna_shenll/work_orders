<?php
include("header.php");
?>
<form name="labour_form" id="labour_form" method="post" action="">
<input type="hidden" name="hdncoachid" value="<?php echo $_GET['id']; ?>">
<div class="page-content" id="contact-list-page-content">
    <div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="portlet light customlistminheight">
                <div class="portlet-title" >
                    <div class="caption font-dark caption-new">
                        <img src="../assets/layouts/layout2/img/new-order_active.png" class="icon-img">
                        <span class="caption-subject bold uppercase icon-title-name">Create New Work Order</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                 <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcontactsearch" id="contactListresponsive">
                            <div class="col-md-8 col-sm-8 col-xs-12 inputpaddingbottom">
                                <div class="col-md-2 col-sm-4 col-xs-12">
                                    <label><strong>Order No:</strong></label>
                                </div>
                                <div class="col-md-5 col-sm-4 col-xs-12">
                                    <input type="text" name="order_no" id="order_no" class="form-control" placeholder="Order No" disabled value="6">
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12 inputpaddingbottom">
                                <div class="col-md-2 col-sm-4 col-xs-12">
                                    <label><strong>Today's Date:</strong></label>
                                </div>
                                <div class="col-md-5 col-sm-4 col-xs-12">
                                    <input type="text" name="today_date" id="today_date" class="form-control" placeholder="Today's Date" value="<?php echo date("m/d/Y")?>" disabled>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12 inputpaddingbottom">
                                <div class="col-md-2 col-sm-4 col-xs-12" style="padding-right: 0px;">
                                    <label><strong>Required Date:</strong></label>
                                </div>
                                <div class="col-md-5 col-sm-4 col-xs-12">
                                    <input type="text" name="required_date" id="required_date" class="form-control" placeholder="mm/dd/yyyy" value="">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 contact-add-padding">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label><strong>Item</strong></label>
                                <input type="text" name="item_id[]" id="item_id[]" class="form-control" placeholder="Item ID" value="">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label style="color:#ffffff"><strong>-</strong></label>
                                <input type="text" name="quantity[]" id="quantity[]" class="form-control" placeholder="Quantity" value="">
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <label><strong>Material Required</strong></label>
                                <input type="text" name="part[]" id="part[]" class="form-control" placeholder="part #" value="">
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                 <label style="color:#ffffff"><strong>-</strong></label>
                                <select  name="select_type[]" id="select_type[]" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Semi Finish" selected>SF</option>
                                    <option value="Raw Material">RM</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <label style="color:#ffffff"><strong>-</strong></label>
                                <input type="text" name="production[]" id="production[]" class="form-control" placeholder="Production Quantity" value="">
                            </div>
                            <div id="main">
                            </div>
                            <div class="col-md-3" style="margin-top:20px;">
                                <button type="button" class="btn custombtn btn-success" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add</button>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:20px;">
                            <div class="col-md-5 col-sm-4 col-xs-12 inputpaddingbottom">
                                <label><strong>Special Note:</strong></label>
                                <textarea class="form-control" name="special_note" id="special_note" rows="7" cols="7"></textarea> 
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 contact-add-padding">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <a href="../admin/dashboard" class="btn grey-mint custombtn" style="width:92px;"><i class="fa fa-arrow-circle-left"></i>&nbsp;Go Back</a>
                                <a href="../admin/dashboard">
                                <button type="button" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Delete</button></a>
                                <a href="../admin/dashboard" class="btn green custombtncolor custombtn"><i class="fa fa-floppy-o"></i> Save</a>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
<script>
$(document).ready(function() {
    i=1;
    $('#addmore').on('click', function(){
        var newfield = '<div class="col-md-12 col-sm-12 col-xs-12" style="padding-left:0px;padding-right:0px;padding-top:15px;"><div class="col-md-2 col-sm-2 col-xs-12"><input type="text" name="item_id[]'+i+'" id="item_id_'+i+'" class="form-control" placeholder="Item ID" value=""></div><div class="col-md-2 col-sm-2 col-xs-12"><input type="text" name="quantity[]'+i+'" id="quantity_'+i+'" class="form-control" placeholder="Quantity" value=""></div><div class="col-md-2 col-sm-2 col-xs-12"><input type="text" name="part[]'+i+'" id="part_'+i+'" class="form-control" placeholder="part #" value=""></div><div class="col-md-3 col-sm-3 col-xs-12"><select  name="select_type[]'+i+'" id="select_type_'+i+'" class="form-control"><option value="">Select</option><option value="Semi Finish" selected>SF</option><option value="Raw Material">RM</option></select></div><div class="col-md-3 col-sm-3 col-xs-12"><input type="text" name="production[]'+i+'" id="production_'+i+'" class="form-control" placeholder="Production Quantity" value=""></div>';
        $('#main').append(newfield);
        // var answers =$('#link_'+i).val();
        // if(answers=="")
        // {
        //     $(document).find('#link_'+i).rules('add', {
        //       required: true,
        //       messages: {
        //         required: 'Please enter link'
        //       }
        //     });
        // }
       i++; 
    });
    $(document).on('click','#remove', function(){
        $(this).parent().parent('div').remove();
    });
   i++; 
});
</script>
<script>
$(function() {
    $("#required_date" ).datepicker({
       autoclose: true,
       todayHighlight: true,
       dateFormat: 'mm/dd/yy'});
    
  });
</script>

<?php
// include_once('../includes/configure.php');
// include_once('../api/Common.php');
// include_once('../includes/session_check.php');
// if ($_SESSION['ADMIN_TYPE'] != "admin") {
//     header("Location:dashboard");
// }
// $Page = 1; $RecordsPerPage = 25;
// $TotalPages = 0;
// $commonAppApi = new Common($dbconn);
// if (isset($_POST["labourSearchCriteria"])) {
//     $labourSearch = json_decode($_POST["labourSearchCriteria"], true);
//     $searchName = trim(!empty($labourSearch["searchName"])) ? trim($labourSearch["searchName"]) : "" ; 
//     $searchEmail = trim(!empty($labourSearch["searchEmail"])) ? trim($labourSearch["searchEmail"]) : "" ;
//     $searchStatus = $labourSearch["searchStatus"];
//     if (isset($labourSearch['HdnPage']) && is_numeric($labourSearch['HdnPage']))
//         $Page = $labourSearch['HdnPage'];
// }  
?>
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>"> 
<div class="portlet-body">
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="tbl_coach_list">
            <thead>
                <tr class="table-view-back-color-design">
                    <th><input type="checkbox" name="export_all" onclick="checkAllExp(this)"></th>
                    <th>#</th>
                    <th title="FIRST NAME">First</th>
                    <th title="LAST NAME">Last</th>
                    <th title="EMAIL">Email</th>
                    <th title="PHONE NUMBER">Phone</th>
                    <th title="STATUS">Status</th>
                    <th class="text-center" title="Action">Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="checkbox-column">
                      <input type="checkbox" name="checkbox[]" id="checkbox" value="<?php echo $labourListData["user_id"];?>" onclick="eachChk()">
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="lasttd" style="text-align: -webkit-center;">
                        <table>
                            <tr>
                                <td>
                                   <a href="../admin/labour?id=<?php echo $commonAppApi->encode($labourListData["user_id"]); ?>" data-toggle="tooltip"
                                       title='Edit' class="color-tooltip">
                                       <button type="button" class="btn btn-info task-edit-comment" >
                                           <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                       </button>
                                    </a>
                                </td>
                                <td>
                                   <a href="../admin/labours?id=<?php echo $commonAppApi->encode($labourListData["user_id"]); ?>" onclick="return confirm('Are you sure want to delete this labour?')" data-toggle="tooltip" 
                                       title='Delete' class="color-tooltip">
                                      <button type="button" class="btn btn-info task-edit-comment">
                                          <i class="fa fa-trash-o" aria-hidden="true"></i>
                                       </button>
                                    </a>
                                </td>
                            </tr>
                        </table>   
                    </td>
                </tr>
                <tr>
                    <td colspan="12" class="child hiddentablerow" style="padding-top: 0px;padding-bottom: 0px;">
                        <div class="accordian-body collapse firstInner_<?php echo $contactListData["user_id"]; ?>" id="<?php echo $sno ?>" style="padding:8px;">
                            <div class="loadinginnersection">
                            <img src="../assets/layouts/layout2/img/loading-publish.gif" alt="loadingimage" id="loadingimage">
                            </div>
                        </div> 
                    </td>
                </tr>
                <?php $sno++;     
                        }
                    }
                } else { ?>
                    <tr><td colspan="8" class="text-center">No record(s) found</td></tr>
                <?php }
                ?>
            </tbody>
        </table>
    </div>
<?php if ($TotalPages > 1) {
        echo "<tr><td style='text-align:center;overflow:none;' colspan='8' valign='middle' class='pagination'>";
        $FormName = "labur_list_form";
        require_once ("paging.php");
        echo "</td></tr>";
    }
?>
</div>
<!-- <script src="../assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script> -->
<script>
    $(document).ready(function(){
       $('[data-toggle="tooltip"]').tooltip();   
    });

    function checkAllExp(Element)
    {
        chk_len=$('input[id=checkbox]').length;
        if(Element.checked){
            if(chk_len>0){
                $('input[id=checkbox]').prop('checked', true);
            }
        }
        else{
            if(chk_len>0){
                $('input[id=checkbox]').prop('checked', false);
            }
        }
    }
    function eachChk()
    {
        chk_len=$('input[id=checkbox]').length;
        chk_len1=$('input[id=checkbox]:checked').length;
        if(chk_len==chk_len1){
            $('input[name=export_all]').prop('checked', true);
        }
        else{
            $('input[name=export_all]').prop('checked', false);
        }
    }

    $(document).ready(function(){
    
    $("#delete").click(function(){
        
        chk_len = $('input[id=checkbox]:checked').length;

        if(chk_len==0)
        {
            alert("Please select atleast one faq");
            return false;
        }

        else
        {       
            $("#frm_service_list").submit();
        }
   });
});

</script>
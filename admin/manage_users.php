<?php
include("header.php");
?>
<form name="customerlist_form" id="customerlist_form" method="post" action="" class="form-horizontal">
<!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row food-orders">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light customlistminheight">
                    <div class="portlet-title" >
                         <div class="caption font-dark caption-new">
                            <img src="../assets/layouts/layout2/img/labours-25x25-1.png" class="icon-img">
                            <span class="caption-subject bold uppercase icon-title-name">Add User</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">First Name:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="First Name" id="First Name" class="form-control" placeholder="First Name" value="Avalon">
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Last Name:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="Last Name" id="Last Name" class="form-control" placeholder="Last Name" value="Roberts ">
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email:</label>
                                    <div class="col-md-4">
                                        <input type="text" name="Email" id="Email" class="form-control" placeholder="Email Address" value="avalon@gmail.com">
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Password:</label>
                                    <div class="col-md-4">
                                        <input type="password" name="Password" id="Password" class="form-control" placeholder="Password" value="123456">
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Confirm Password:</label>
                                    <div class="col-md-4">
                                        <input type="Password" name="Password" id="Password" class="form-control" placeholder="Confirm Password" value="123456">
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">User Type:</label>
                                    <div class="col-md-4">
                                        <select  name="User Type" id="User Type" class="form-control" placeholder="User Type">
                                            <option value="">Select</option>
                                            <option value="Admin" selected>Admin</option>
                                            <option value="Super-Admin">Super-Admin</option>
                                            <option value="User">User</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Status:</label>
                                    <div class="col-md-4">
                                        <div class="mt-radio-inline">
                                            
                                            <label class="mt-radio">
                                                <input type="radio" name="optionsRadios" id="optionsRadios25" value="option2" checked=""> Active
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="optionsRadios" id="optionsRadios26" value="option1" checked="">In-Active 
                                                <span></span>
                                            </label>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions top">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="users_list" class="btn green custombtn custombtncolor " id="Search"><i class="fa fa-floppy-o"></i> Save</a>
                                        <a href="users_list" class="btn red custombtn " id="Search"><i class="fa fa-times-circle"></i> Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include_once("footer.php"); ?>
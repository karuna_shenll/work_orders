<?php
include("header.php");
?>
<form name="labour_form" id="labour_form" method="post" action="">
<input type="hidden" name="hdncoachid" value="<?php echo $_GET['id']; ?>">
<div class="page-content" id="contact-list-page-content">
    <div class="row food-orders">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="portlet light customlistminheight">
                <div class="portlet-title" >
                    <div class="caption font-dark caption-new">
                        <img src="../assets/layouts/layout2/img/print_active.png" class="icon-img">
                        <span class="caption-subject bold uppercase icon-title-name">Enter Shop Details</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                 <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 reportcontactsearch" id="contactListresponsive">
                            <div class="col-md-8 col-sm-8 col-xs-12 remove-left-right-padding">
                                <div class="col-md-12 col-sm-12 col-xs-12 inputpaddingbottom">
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <label><strong>Work Order:</strong></label>
                                    </div>
                                    <div class="col-md-5 col-sm-4 col-xs-12">
                                        <input type="text" name="order_no" id="order_no" class="form-control" disabled placeholder="Order No" value="2">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 inputpaddingbottom">
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <label><strong>Date Created:</strong></label>
                                    </div>
                                    <div class="col-md-5 col-sm-4 col-xs-12">
                                        <input type="text" name="today_date" id="today_date" class="form-control date-picker" disabled placeholder="Today's Date" value="12/29/2017">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 inputpaddingbottom">
                                    <div class="col-md-3 col-sm-4 col-xs-12" style="padding-right: 0px;">
                                        <label><strong>Date Required:</strong></label>
                                    </div>
                                    <div class="col-md-5 col-sm-4 col-xs-12">
                                        <input type="text" name="required_date" id="required_date" class="form-control date-picker" placeholder="mm/dd/yyyy" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <h4><strong>MATERIAL REQUIREMENTS<strong></h4>
                                <table class="table table-striped table-bordered table-hover">
                                    <tbody>
                                        <tr>
                                            <th title="FIRST NAME" class="text-center tab_1">MATERIAL</th>
                                            <th title="LAST NAME" class="text-center tab_1">RAW / SF</th>
                                            <th title="EMAIL" class="text-center tab_1">QUANTITY</th>
                                        </tr>
                                        <tr>
                                            <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                            <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                            <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        </tr>
                                        <tr>
                                            <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                            <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                            <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 contact-add-padding">
                            <h4><strong>PRODUCTION & SHOP<strong></h4>
                            <table class="table table-striped table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <th class="text-center tab_1">ITEM ID</th>
                                        <th class="text-center tab_1">QUANTITY</th>
                                        <th class="text-center tab_2">START DATE/TIME</th>
                                        <th class="text-center tab_2">MACHINE</th>
                                        <th class="text-center tab_2">OPERATOR NAME</th>
                                        <th class="text-center tab_2">SETUP TIME (MIN)</th>
                                        <th class="text-center tab_2">CYCLE TIME</th>
                                        <th class="text-center tab_2">DATE/TIME COMPLETE</th>
                                        <th class="text-center tab_2"># SENT TO INSEPTION</th>
                                        <th class="text-center tab_2"># SCRAP</th>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="2"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="20"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="5"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="50"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="7"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="21"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="8"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="25"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 contact-add-padding">
                            <h4><strong>INSPECTION DEPARTMENT<strong></h4>
                            <table class="table table-striped table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <th class="text-center tab_3">ITEM ID</th>
                                        <th class="text-center tab_3"># RECEIVED FROM PRODUCTION</th>
                                        <th class="text-center tab_3">DATE/TIME RECEIVED</th>
                                        <th class="text-center tab_3">COATING TYPE</th>
                                        <th class="text-center tab_3"># SCRAP</th>
                                        <th class="text-center tab_3">NOTES</th>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="4"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="1"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                    <tr>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="5"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value="3"></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control form_datetime" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                        <td><input type="text" name="required_date" id="required_date" class="form-control" value=""></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 contact-add-padding">
                            <div class="col-md-6 col-sm-6 col-xs-6 contact-save">
                                <a href="shop_details" class="btn yellow custombtn"><i class="fa fa-floppy-o"></i> Save</a>
                                <style type="text/css">
                                    .custombtn{width:95px;}
                                    .tab_1{background-color: #FCE4D6;}
                                    .tab_2{background-color: #E2EFDA;}
                                    .tab_3{background-color: #D9E1F2;}
                                    .form-control{font-weight: 400;}
                                </style>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <a href="shop_details" class="btn red custombtn" value="reset" name="reset" id="customReset"><i class="fa fa-times-circle"></i> Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
<?php include_once("footer.php"); ?>
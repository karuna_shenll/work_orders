-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 27, 2017 at 01:21 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `business_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jobs`
--

CREATE TABLE `tbl_jobs` (
  `job_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_date` date NOT NULL,
  `job_time` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jobs`
--

INSERT INTO `tbl_jobs` (`job_id`, `user_id`, `job_date`, `job_time`, `status`, `created_date`) VALUES
(1, 2, '2017-12-15', '3:14:10 AM', 0, '2017-12-13 07:24:13'),
(2, 3, '2017-12-20', '7:30:15 AM', 0, '2017-12-13 08:30:17'),
(3, 4, '2017-12-28', '07:30:45', 2, '2017-12-13 08:31:44'),
(4, 5, '2017-12-31', '9:24:45 AM', 0, '2017-12-13 03:24:42'),
(5, 5, '2017-12-05', '11:08:45 AM', 0, '2017-12-14 03:38:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `first` varchar(100) NOT NULL,
  `last` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(250) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(150) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `avatar` varchar(250) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `first`, `last`, `gender`, `email`, `password`, `type`, `phone`, `city`, `state`, `status`, `avatar`, `created_date`, `modified_date`) VALUES
(1, 'admin', 'admin', 'M', 'admin@test.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'admin', '9626943111', '', '', 1, NULL, '2017-12-13 03:27:31', NULL),
(2, 'Indhiyan', 'V', 'M', 'indhiyan.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'labour', '9629715190', '', '', 1, NULL, '2017-12-13 03:27:31', NULL),
(3, 'Thiyagarajan', 'SR', 'M', 'thiyagu.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'labour', '9626943111', '', '', 1, NULL, '2017-12-13 03:27:31', NULL),
(4, 'Vigneshkumar', 'R', 'M', 'vigneshkumar.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'labour', '9874561230', '', '', 1, NULL, '2017-12-13 03:27:31', NULL),
(5, 'Ramani', 'M', 'M', 'ramani.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'labour', '9874561230', '', '', 1, NULL, '2017-12-13 03:27:31', NULL),
(6, 'Karuna', 'C', 'M', 'karuna.shenll@gmail.com', 'WqxheqzkzBMn7-gbD9K0Jj4C70HxT8g8cyj4BYyYL7k', 'labour', '9874561230', '', '', 1, NULL, '2017-12-13 03:27:31', NULL),
(7, 'test', 'test', 'M', 'test.shenll@gmail.com', '', 'labour', '9874563214', '', '', 2, NULL, '2017-12-13 08:54:01', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_jobs`
--
ALTER TABLE `tbl_jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_jobs`
--
ALTER TABLE `tbl_jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

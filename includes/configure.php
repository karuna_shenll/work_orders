<?php
session_start();
error_reporting(0);
global $dbconn;
define("LOCAL_SERVER_NAME","localhost");
define("DEMO_SERVER_NAME","demo.shenll.net");

//PDO connection starts here
try {
	if (substr_count($_SERVER["SERVER_NAME"], LOCAL_SERVER_NAME) > 0 ) {
		// Database Informations
		!defined("DB_HOST_NAME") ? define("DB_HOST_NAME","localhost") : "" ;
		!defined("DATABASE") ? define("DATABASE","business_app") : "" ;
		!defined("USERNAME") ? define("USERNAME","root") : "" ;
		!defined("PASSWORD") ? define("PASSWORD","") : "" ;
		define("LAW_ROOT_PATH",$_SERVER["DOCUMENT_ROOT"]."/business/");
		define("LAW_SYSTEM_PATH","http://localhost/business/");
	} else if (substr_count($_SERVER["SERVER_NAME"], DEMO_SERVER_NAME) > 0 ) {
		// Database Informations
		!defined("DB_HOST_NAME") ? define("DB_HOST_NAME","lawproapp.db.6877569.09e.hostedresource.net") : "" ;
		!defined("DATABASE") ? define("DATABASE","lawproapp") : "" ;
		!defined("USERNAME") ? define("USERNAME","lawproapp") : "" ;
		!defined("PASSWORD") ? define("PASSWORD","L@wProApp2018") : "" ;
		define("LAW_ROOT_PATH",$_SERVER["DOCUMENT_ROOT"]."/demo.shenll.net/business/");
		define("LAW_SYSTEM_PATH","http://demo.shenll.net/business/");
	}
	// Mysql PDO connection
	$dbconn = new PDO("mysql:host=".DB_HOST_NAME.";dbname=".DATABASE,USERNAME,PASSWORD);
	$dbconn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
} catch (PDOException $e) {
   echo "Connection failed: " . $e->getMessage();
}


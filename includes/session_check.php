<?php
	if(!isset($_SESSION['ADMIN_ID']) || trim($_SESSION['ADMIN_ID']) == "") {
		session_destroy();
		header("Location: index.php");
		exit;
	}
?>
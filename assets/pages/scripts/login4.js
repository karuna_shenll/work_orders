var Login = function() {
    var e = function() {
            $(".login-form").validate({
                errorElement: "span",
                errorClass: "help-block",
                focusInvalid: !1,
                rules: {
                    email: {
                        required: !0
                    },
                    password: {
                        required: !0
                    },
                    remember: {
                        required: !1
                    }
                },
                messages: {
                    email: {
                        required: "Email is required."
                    },
                    password: {
                        required: "Password is required."
                    }
                },
                invalidHandler: function(e, r) {
                    $(".alert-danger", $(".login-form")).show()
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-error"), e.remove()
                },
                errorPlacement: function(e, r) {
                    e.insertAfter(r.closest(".input-icon"))
                },
                submitHandler: function(e) {
                    e.submit()
                }
            }), $(".login-form input").keypress(function(e) {
                return 13 == e.which ? ($(".login-form").validate().form() && $(".login-form").submit(), !1) : void 0
            })
        },
        r = function() {
            $(".forget-form").validate({
                errorElement: "span",
                errorClass: "help-block",
                focusInvalid: !1,
                ignore: "",
                rules: {
                    forgetemail: {
                        required: !0,
                        email: !0
                    }
                },
                messages: {
                    forgetemail: {
                        required: "Email is required."
                    }
                },
                invalidHandler: function(e, r) {},
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-error"), e.remove()
                },
                errorPlacement: function(e, r) {
                    e.insertAfter(r.closest(".input-icon"))
                },
                submitHandler: function(e) {
                    e.submit()
                }
            }), $(".forget-form input").keypress(function(e) {
                return 13 == e.which ? ($(".forget-form").validate().form() && $(".forget-form").submit(), !1) : void 0
            }), jQuery("#forget-password").click(function() {
                jQuery(".login-form").hide(), jQuery(".forget-form").show()
            }), jQuery("#back-btn").click(function() {
                jQuery(".login-form").show(), jQuery(".forget-form").hide()
            })
        };
    return {
        init: function() {
            e(), r()
        }
    }
}();
jQuery(document).ready(function() {
    Login.init()
});
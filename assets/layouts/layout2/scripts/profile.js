$("form[name='form_changepassword']").validate({
    rules: {
      current_pass:{
      	required: true,
        unique:true
      },
      password:{
        required: true
      },
      conpassword:{required: true,equalTo: "#password"},
     
    },
    messages: {
      current_pass:{
      	required:"Please enter current password",
        unique:"Invalid your current password"
      },
      password:{required:"Please enter password"},
      conpassword:
      {
        required:"Please enter confirm password",
        equalTo: "Your password and re-type new password do not match"
      },
     
    },
    submitHandler: function(form) {
      form.submit();
    }
});
$("form[name='form_proflie']").validate({
    rules: {
      first_name:{
        required: true
      },
      last_name:{
        required: true
      },
      firmname:{
         required:true
      },
      email:{required: true,email:true},
      phone:{
        required:true
      },
      address:{
        required:true
      },
      city:{
        required:true
      },
      state:{
        required:true
      },
     
    },
    messages: {
      first_name:{
        required:"Please enter first name",
      },
      last_name:{
        required:"Please enter last name",
      },
      firmname:{
         required:"Please select firm name"
      },
      email:
      {
        required:"Please enter email",
        email: "Please enter valid email"
      },
      phone:{
        required:"Please enter mobile number",
      },
       address:{
        required:"Please enter address",
      },
      city:{
        required:"Please enter city",
      },
      state:{
        required:"Please select state",
      }
     
    },
    submitHandler: function(form) {
      form.submit();
    }
});
function showimagepreview(input,inputid) {
	if (input.files && input.files[0]) {
		var filerdr = new FileReader();
		filerdr.onload = function(e) {
		$('.imgpreview').attr('src', e.target.result);
		$('.imgpreview').css({"width": "200pxpx", "height": "150px"});
		}
		filerdr.readAsDataURL(input.files[0]);
	}
}

$.validator.addMethod('unique',function(value)
    {
        var current_pass=$('input[name="current_pass"]').val();
        var user_id=$('input[name="hnduserid"]').val();
        var result=$.ajax({
                    type:"POST",
                    async:false,
                    url:"commonajax.php",
                    data:{"currentpass":current_pass,"userid":user_id},
                    dataType:"text"});
                    //console.log(result.responseText);
     if(result.responseText=="exists"){
         return true;
     }
     else{ 
         return false;
     }
     },
     "Invalid your current password");
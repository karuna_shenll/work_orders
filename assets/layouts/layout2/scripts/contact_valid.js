$(function() {
  $("form[name='job_form']").validate({
    rules: {
      client_name: "required",
      contact_first: "required",
      contact_last: "required",
      email: {
        required: true,
        email: true
      },
      gender: "required",
      city: "required",
      state: "required",
      address: "required",
      comment: "required",
      telephone: "required",
      status: "required",
    },
    messages: {
      client_name: "Please select your client",
      contact_first: "Please enter your first name",
      contact_last: "Please enter your last name",
      email: "Please enter a valid email address",
      gender: "Please select gender",
      city: "Please enter your city",
      state: "Please select your state",
      address: "Please enter your address",
      comment: "Please enter your comment",
      telephone: "Please enter your telephone number",
      status: "Please choice status",
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

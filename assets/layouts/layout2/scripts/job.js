$(document).ready(function(){

    var projectName = new proApp();
    var jobArr = {};
    jobArr["searchName"] = $("#search_name").val();
    jobArr["HdnPage"] = $("#HdnPage").val();
    jobArr["HdnMode"] = $("#HdnMode").val();
    jobArr["RecordsPerPage"] = $("#RecordsPerPage").val();
    var jobJsonString = JSON.stringify(jobArr); 
    projectName.jobList(jobJsonString);

    $(document).on("click", "#Search", function() {

        jobArr["searchName"] = $("#search_name").val();
        jobArr["HdnPage"] = $("#HdnPage").val();
        jobArr["HdnMode"] = $("#HdnMode").val();
        jobArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var jobJsonString = JSON.stringify(jobArr);
        projectName.jobList(jobJsonString);
    });

    $(document).on("click", "#customReset", function() {

        $("#searchName").val("");
        $("#HdnPage").val("1");
        $("#HdnMode").val("1");
        $("#RecordsPerPage").val("25");

        jobArr["searchName"] = $("#search_name").val();
        jobArr["HdnPage"] = $("#HdnPage").val();
        jobArr["HdnMode"] = $("#HdnMode").val();
        jobArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var jobJsonString = JSON.stringify(jobArr);
        projectName.jobList(jobJsonString);
    });

    // //$(document).on("click", ".getclienttask", function() {
    // $(document).ready(function(){
    //     var clientTaskDetArr = {};
    //     //var client_id = $(this).attr("data-client-id");
    //     var client_id = $("#hndclientid").val();
    //     if(client_id!=""){
    //         clientTaskDetArr["client_id"] = client_id;
    //         var clientTaskDetJsonString = JSON.stringify(clientTaskDetArr);
    //         projectName.getClientTaskDet(clientTaskDetJsonString,client_id);
    //     }
    // });
});
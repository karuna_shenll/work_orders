$(document).ready(function(){

    var projectName = new proApp();
    
    var contactDetArr = {};
    contactDetArr["contact_id"] = $("#contact").val();
    contactDetArr["email"] = $("#email").val();
    contactDetArr["status"] = $("#status").val();
    contactDetArr["HdnPage"] = $("#HdnPage").val();
    contactDetArr["HdnMode"] = $("#HdnMode").val();
    contactDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
    var contactDetJsonString = JSON.stringify(contactDetArr); 

    projectName.contactList(contactDetJsonString);

    $(document).on("click", "#Search", function() {

        contactDetArr["contactname"] = $("#contact").val();
        contactDetArr["email"] = $("#email").val();
        contactDetArr["status"] = $("#status").val();
        contactDetArr["HdnPage"] = $("#HdnPage").val();
        contactDetArr["HdnMode"] = $("#HdnMode").val();
        contactDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var contactDetJsonString = JSON.stringify(contactDetArr);
        projectName.contactList(contactDetJsonString);
    });

    $(document).on("click", "#customReset", function() {

        $("#contact").val("");
        $("#email").val("");
        $("#status").val("");
        $("#HdnPage").val("1");
        $("#HdnMode").val("1");
        $("#RecordsPerPage").val("25");

        contactDetArr["contactname"] = $("#contact").val();
        contactDetArr["email"] = $("#email").val();
        contactDetArr["status"] = $("#status").val();
        contactDetArr["HdnPage"] = $("#HdnPage").val();
        contactDetArr["HdnMode"] = $("#HdnMode").val();
        contactDetArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var contactDetJsonString = JSON.stringify(contactDetArr);
        projectName.contactList(contactDetJsonString);
    });

    $(document).on("click","#popupStatus",function() {
        var contactid = $(this).attr("data-id");
        $("#popstatus").val("");
        if (contactid!="") {
            $("#contactID").val(contactid);
            $("#contactStatus").modal("show");
        } else {
            $("#contactID").val("");
            $("#contactStatus").modal("hide");
        }

    });
    $(document).on("click","#PopupStatus",function() {

        var contactID=$("#contactID").val();
        var popstatus=$("#popstatus").val();

        if(popstatus==""){
            $(".statusmsg").show();
            return false;
        }
        if(popstatus!="" && contactID!="") {
             projectName.contactAjaxStatus(popstatus,contactID);
        }

    });
    $(document).on("change","#popstatus",function() {
         var popstatus=$("#popstatus").val();

        if(popstatus==""){
            $(".statusmsg").show();
            return false;
        } else {
            $(".statusmsg").hide();
            return true;
        }
    });
});
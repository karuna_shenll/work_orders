$(function() {
  $("#labour_form").validate({
    rules: {
      labour_first: "required",
      labour_last: "required",
      labour_email: {
        required: true,
        labour_email: true
      },
      password: {
        required: true
      },
      con_pass:{required: true,equalTo: "#password"},
      gender: "required",
      phone: "required",
      status: "required",
    },
    messages: {
      labour_first: "Please enter your first name",
      labour_last: "Please enter your last name",
      labour_email: {required:"Please enter your email",labour_email:"Please enter a valid email"},
      password: {required:"Please enter password"},
      con_pass: {
        required:"Please enter confirm password",
        equalTo: "Your password and confirmation password do not match"
      },
      gender: "Please select gender",
      phone: "Please enter your phone number",
      status: "Please choice status",
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

$(function() {
  $("#job_form").validate({
    rules: {
      labour_id: "required",
      job_date: "required",
      job_time: "required",
    },
    messages: {
      labour_id: "Please select labour name",
      job_date: "Please enter job date",
      job_time: "Please enter job time",
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});
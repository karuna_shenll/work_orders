$(document).ready(function() {
    var projectName = new proApp();
    var labourArr = {};
    labourArr["search_name"]  = $("#search_name").val();
    labourArr["search_status"] = $("#search_status").val();
    labourArr["HdnPage"] = $("#HdnPage").val();
    labourArr["HdnMode"] = $("#HdnMode").val();
    labourArr["RecordsPerPage"] = $("#RecordsPerPage").val();
    var labourArrJsonString = JSON.stringify(labourArr);
    projectName.labourList(labourArrJsonString);

    $(document).on("click", "#Search", function() {

        labourArr["searchName"] = $("#search_name").val();
        labourArr["searchEmail"] = $("#search_email").val();
        labourArr["searchStatus"] = $("#search_status").val();
        labourArr["HdnPage"] = $("#HdnPage").val();
        labourArr["HdnMode"] = $("#HdnMode").val();
        labourArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var labourArrJsonString = JSON.stringify(labourArr);
        projectName.labourList(labourArrJsonString);
    });

    $(document).on("click", "#customReset", function() {

        $("#search_name").val("");
        $("#search_email").val("");
        $("#search_status").val("");
        $("#HdnPage").val("1");
        $("#HdnMode").val("1");
        $("#RecordsPerPage").val("25");

        labourArr["searchName"] = $("#search_name").val();
        labourArr["searchEmail"] = $("#search_email").val();
        labourArr["searchStatus"] = $("#search_status").val();
        labourArr["HdnPage"] = $("#HdnPage").val();
        labourArr["HdnMode"] = $("#HdnMode").val();
        labourArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var labourArrJsonString = JSON.stringify(labourArr);
        projectName.labourList(labourArrJsonString);
    });

    $(document).ready(function() {
    // $(document).on("click", ".getclienttask", function() {
        var clientTaskDetArr = {};
        var clientId = $("#hndclientid").val();
        var coachId = $("#hndcoachid").val();
        if (clientId!="") {
           //var clientId = $(this).attr("data-client-id");
            clientTaskDetArr["clientid"] = clientId;
            clientTaskDetArr["coachid"] = coachId;
            var clientTaskDetJsonString = JSON.stringify(clientTaskDetArr);
            projectName.ClientTaskDetList(clientTaskDetJsonString,clientId);
        }
        
    });
});
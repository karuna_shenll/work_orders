$(document).ready(function() {
    var projectName = new proApp();
    var activeArr = {};
    activeArr["search_name"]  = $("#search_name").val();
    activeArr["search_status"] = $("#search_status").val();
    activeArr["HdnPage"] = $("#HdnPage").val();
    activeArr["HdnMode"] = $("#HdnMode").val();
    activeArr["RecordsPerPage"] = $("#RecordsPerPage").val();
    var labourArrJsonString = JSON.stringify(activeArr);
    projectName.activeList(labourArrJsonString);

    $(document).on("click", "#Search", function() {

        activeArr["searchName"] = $("#search_name").val();
        activeArr["searchEmail"] = $("#search_email").val();
        activeArr["searchStatus"] = $("#search_status").val();
        activeArr["HdnPage"] = $("#HdnPage").val();
        activeArr["HdnMode"] = $("#HdnMode").val();
        activeArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var labourArrJsonString = JSON.stringify(activeArr);
        projectName.activeList(labourArrJsonString);
    });

    $(document).on("click", "#customReset", function() {

        $("#search_name").val("");
        $("#search_email").val("");
        $("#search_status").val("");
        $("#HdnPage").val("1");
        $("#HdnMode").val("1");
        $("#RecordsPerPage").val("25");

        activeArr["searchName"] = $("#search_name").val();
        activeArr["searchEmail"] = $("#search_email").val();
        activeArr["searchStatus"] = $("#search_status").val();
        activeArr["HdnPage"] = $("#HdnPage").val();
        activeArr["HdnMode"] = $("#HdnMode").val();
        activeArr["RecordsPerPage"] = $("#RecordsPerPage").val();
        var labourArrJsonString = JSON.stringify(activeArr);
        projectName.activeList(labourArrJsonString);
    });

    // $(document).ready(function() {
    // // $(document).on("click", ".getclienttask", function() {
    //     var clientTaskDetArr = {};
    //     var clientId = $("#hndclientid").val();
    //     var coachId = $("#hndcoachid").val();
    //     if (clientId!="") {
    //        //var clientId = $(this).attr("data-client-id");
    //         clientTaskDetArr["clientid"] = clientId;
    //         clientTaskDetArr["coachid"] = coachId;
    //         var clientTaskDetJsonString = JSON.stringify(clientTaskDetArr);
    //         projectName.ClientTaskDetList(clientTaskDetJsonString,clientId);
    //     }
        
    // });
});
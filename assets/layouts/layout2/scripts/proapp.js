function proApp () {        
}

proApp.prototype.labourList = function (labourDetJsonString) {
    $("#labur_list_table").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "labour_listing.php",
        data: {labourSearchCriteria: labourDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#labur_list_table").empty().append(response);
            $(".loadingreportsection").hide();
            $("#labur_list_table").show();
        }, 
    })
}

proApp.prototype.jobList = function (jobDetJsonString) {
    $("#labur_list_table").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "job_listing.php",
        data: {jobDetJsonString: jobDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#joblisttable").empty().append(response);
            $(".loadingreportsection").hide();
            $("#joblisttable").show();
        }, 
    })
}

proApp.prototype.activeList = function (activeDetJsonString) {
    $("#active_list_table").hide();
    $(".loadingreportsection").show();    
    $.ajax({
        method: "POST",
        url: "deactive_listing.php",
        data: {labourSearchCriteria: activeDetJsonString},
        error: function(){
            $("#loadingimage").hide();
        },
        success: function(response) {
            $("#active_list_table").empty().append(response);
            $(".loadingreportsection").hide();
            $("#active_list_table").show();
        }, 
    })
}

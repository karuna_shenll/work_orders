<?php
class Common {

    public $dbconn;
    public function __construct(PDO $dbconn){
        $this->dbconn = $dbconn;
    }
    /*Backend API*/
	/*Function to execute the SELECT queries*/
    public function funBckendExeSelectQuery($qry, $qryParams = array(), $fetchtype=''){        
        $prepareQry = $this->dbconn->prepare($qry);
        if (count($qryParams)>0)            // Fetching all records
            $execQry =  $prepareQry->execute($qryParams);
        else                                // Fetching one record
            $execQry = $prepareQry->execute();
            $row_count = $prepareQry->rowCount();

        if ($row_count>0 && $fetchtype=='') {
			$fetch_data = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
            return $fetch_data;
        } elseif ($row_count>0 && $fetchtype=='fetch') {
            $fetch_data = $prepareQry->fetch(PDO::FETCH_ASSOC);
            return $fetch_data;
        }
        else {
            return false;
        }
    }   

    /*Function to execute the email valid*/
    public function valid_email ($email) {
        if(is_array($email) || is_numeric($email) || is_bool($email) || is_float($email) || is_file($email) || is_dir($email) || is_int($email)) {
            return false;
        } else {
            $email=trim(strtolower($email));
            if(filter_var($email, FILTER_VALIDATE_EMAIL)!==false) {
                return true;
            } else {
                $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
                return (preg_match($pattern, $email) === 1) ? $email : false;
            }
        }
    }

    /*Function to execute the Insert queries*/
    function funBckendExeInsertRecord($query,$insert_data) {
        $prepareInsertQry = $this->dbconn->prepare($query);
        $ExecPrepInsertQry = $prepareInsertQry->execute($insert_data);
        if($ExecPrepInsertQry) {
            $last_insert_id = $this->dbconn->lastInsertId();
            return $last_insert_id;
        } else {
            return false;
        }
    }

    /*Function to execute the update queries*/
    function funBckendExeUpdateRecord($qry, $qryParams) {
        $PrepUpdateQry =  $this->dbconn->prepare($qry);
        $ExecPrepUpdateQry = $PrepUpdateQry->execute($qryParams);
        if ($ExecPrepUpdateQry) {
            return true;
        } else{
            return false;
        }
    }

    public function funBckendExeTotalPotentialQuery ($userId) {
        $total = "";
        if ($userId) {
            $tasksQry = "SELECT sum(total_potential) as total_potential FROM pro_pipelines WHERE user_id= $userId";
            $totalpipeline = $this->funBckendExeSelectQuery($tasksQry,$qryParams);
            $total = $totalpipeline[0]["total_potential"];
            if ($total == "") {
                $total = 0;
            }
        }
        return $total;
    }   

    public function funBckendExeCoachTotalPotentialQuery ($userId) {
        $total = "";
        if ($userId) {

            $selQryParams = array (":status" => 1, ":type" => "contact", ":coach_id" => $userId);
            $whereCondtn = $this->funParseQryParams($selQryParams, "coach_id", "AND");
            $reqQryParams = array ("fetchType" => "multiple", "selectField" => "", "tableName" => "pro_users", "whereCondition" => $whereCondtn);   

            $selQryResponse = $this->funExeSelectQuery($reqQryParams, $selQryParams);
            if (!empty($selQryResponse) && count($selQryResponse) > 0) {
                $k = 0;
                $user_id = "";
                foreach ($selQryResponse as $key => $value) {
                    $user_id[$k] = $value["user_id"];
                    $k++;
                }
                $user_contact = implode(",",$user_id);

                $Qry = "SELECT sum(total_potential) as total_potential FROM pro_pipelines WHERE user_id in (".$user_contact.") ORDER BY pipe_id DESC";

                $pipelinesInfo = $this->funBckendExeSelectQuery($Qry,$qryParams = array());
                $total = $pipelinesInfo[0]["total_potential"];

                if ($total == "") {
                    $total = 0;
                }
            } else {
                $total = 0;
            }
        }
        return $total;
    }

    public function funBckendExeClientTotalPotentialQuery ($userId) {
        $total = "";
        if ($userId) {

            $selQryParams = array (":status" => 1, ":type" => "contact", ":client_id" => $userId);
            $whereCondtn = $this->funParseQryParams($selQryParams, "client_id", "AND");
            $reqQryParams = array ("fetchType" => "multiple", "selectField" => "", "tableName" => "pro_users", "whereCondition" => $whereCondtn);   

            $selQryResponse = $this->funExeSelectQuery($reqQryParams, $selQryParams);
            if (!empty($selQryResponse) && count($selQryResponse) > 0) {
                $k = 0;
                $user_id = "";
                foreach ($selQryResponse as $key => $value) {
                    $user_id[$k] = $value["user_id"];
                    $k++;
                }
                $user_contact = implode(",",$user_id);

                $Qry = "SELECT sum(total_potential) as total_potential FROM pro_pipelines WHERE user_id in (".$user_contact.") ORDER BY pipe_id DESC";

                $pipelinesInfo = $this->funBckendExeSelectQuery($Qry,$qryParams = array());
                $total = $pipelinesInfo[0]["total_potential"];

                if ($total == "") {
                    $total = 0;
                }
            } else {
                $total = 0;
            }
        }
        return $total;
    }

    // Validation - Check required fields
    public function funCheckRequiredFields ($request, $requiredFieldsArr) {

        $returnMsg = array();

        if (count($requiredFieldsArr) > 0 && count($request) > 0) {

            foreach ($requiredFieldsArr as $field) {

                if (!array_key_exists($field, $request)) {
                    
                    $returnMsg["status"] = 0;
                    $returnMsg["message"] = $field." does not exist";

                } else {

                    if (empty($request[$field])) {
                        
                        $returnMsg["status"] = 0;
                        $returnMsg["message"] = $field." cannot be empty";
                    } else {
                        
                        $returnMsg["status"] = 1;
                        $returnMsg["message"] = "Success";
                    }
                    
                }               

                if (!$returnMsg["status"])
                    break;               
            }
            return $returnMsg;
        }
    }

    /*Function to execulte the SELECT single record queries*/
    public function funExeSelectQuery ($reqQryParams, $qryParams = array()) {

        //create query
        $tableName = $reqQryParams["tableName"];
        $selectCluseCondtn = "*";
        $whereClauseCondtn = ""; 
        $orderByClauseCondtn = ""; 
        $groupByClauseCondtn = "";      

        if (!empty($reqQryParams["selectField"]))
            $selectCluseCondtn = $reqQryParams["selectField"];
        
        if (!empty($reqQryParams["whereCondition"]))
            $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

        if (!empty($reqQryParams["orderByCondtn"]))
                $orderByClauseCondtn = "ORDER BY  ".$reqQryParams["orderByCondtn"];  

        if (!empty($reqQryParams["groupByCondition"]))
                $groupByClauseCondtn = "GROUP BY  ".$reqQryParams["groupByCondition"];         

        $qry = "SELECT $selectCluseCondtn FROM $tableName $whereClauseCondtn $orderByClauseCondtn $groupByClauseCondtn";
        $prepareQry = $this->dbconn->prepare($qry);
        if (!empty($qryParams) && count($qryParams) > 0) {
            $execQry = $prepareQry->execute($qryParams);
        } else {
            $execQry = $prepareQry->execute();
        }
        $rowCnt = $prepareQry->rowCount();
        $fetchData = array();
        if ($rowCnt > 0 ) {
            if (strtolower($reqQryParams["fetchType"]) == "singlerow")
                $fetchData = $prepareQry->fetch(PDO::FETCH_ASSOC);
            else
                $fetchData = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
        }
        return $fetchData;
    }    

    /*Function to execulte the Insert queries*/
    function funExeInsertRecord($tblName, $qryParams = array()) {

        if (!empty($qryParams) && count($qryParams) > 0) {
            // Remove ":" string from the key
            $tblFieldsArr = array();
            array_walk($qryParams, function (&$value,$key) use (&$tblFieldsArr) {
                $tblFieldsArr[ str_replace(":","",$key) ] = $value;
            });

            // retrieve the keys of the array (column titles)
            $tblFields = array_keys($tblFieldsArr);
            $tblValues = array_keys($qryParams);
            // Build the query
            $query = "INSERT INTO ".$tblName." (`".implode('`,`', $tblFields)."`) VALUES (".implode(",", $tblValues).")";
            $prepareInsertQry = $this->dbconn->prepare($query);
            $ExecPrepInsertQry = $prepareInsertQry->execute($qryParams);
            $lastInsertId = "";

            /* Check inserted or not */
            if (!$ExecPrepInsertQry) {
                return false;
            }

            if ($ExecPrepInsertQry)
                $lastInsertId = $this->dbconn->lastInsertId();

            return $lastInsertId;
        }
    }

    /*Function to execulte the update queries*/
    function funExeUpdateRecord($reqQryParams, $qryParams = array()) {

        $tableName = $reqQryParams["tableName"];
        $setClauseCondtn = "";
        $whereClauseCondtn = "";

        if (!empty($reqQryParams["setCondtn"]))
            $setClauseCondtn = "SET ".$reqQryParams["setCondtn"]; 
        
        if (!empty($reqQryParams["whereCondition"]))
            $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

        if (!empty($reqQryParams)) {
            $qry = "UPDATE $tableName $setClauseCondtn $whereClauseCondtn";
            $prepareQry = $this->dbconn->prepare($qry);
            if (!empty($qryParams) && count($qryParams) > 0) {
                $execQry = $prepareQry->execute($qryParams);
            } else {
                $execQry = $prepareQry->execute();
            }
            $rowCnt = $prepareQry->rowCount();
            $updateRes = false;
            if ($rowCnt > 0 ) {
                $updateRes = true;
            }
            return $updateRes;
        }
    }

    /*Function to execulte the delete queries*/
    function funExeDeleteQuery($reqQryParams, $qryParams = array()) {

        //create query
        $tableName = $reqQryParams["tableName"];
        $whereClauseCondtn = ""; 
        if (!empty($reqQryParams["whereCondition"]))
            $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

        $qry = "DELETE FROM $tableName $whereClauseCondtn";
        $PrepDeleteQry = $this->dbconn->prepare($qry);
        $ExecPrepDeleteQry = $PrepDeleteQry->execute($qryParams);
        if($ExecPrepDeleteQry) {
            return true;
        } else{
            return false;
        }
    } 

    // $field and $seperator are required otherwise both are empty
    public function funParseQryParams ($qryParams, $field = "", $seperator = "") {

        $setWhereCondtn = "";
        if (!empty($qryParams) && count($qryParams) > 1) {
            if (!empty($field) && !empty($seperator)) {
                $i = 1;
                foreach ($qryParams as $qryKey => $qryVal) {       
                    $qryKey = str_replace(":","" ,$qryKey); 
                    $setWhereCondtn .= $qryKey." = :".$qryKey;
                    if ($qryKey != $field)
                        $setWhereCondtn .= " $seperator ";
                    $i++;
                }
            } else {
                return "field and seperator required!";
            }
        } else {
            $i = 1;
            foreach ($qryParams as $qryKey => $qryVal) {       
                $qryKey = str_replace(":","" ,$qryKey); 
                $setWhereCondtn .= $qryKey." = :".$qryKey;
                $i++;
            }
        }
        return $setWhereCondtn;
    }

    /* Function to execulte the Leftjoin query */
    public function funExeTwoTblLeftJoinQuery($reqQryParams, $qryParams = array()) {

        if (is_array($reqQryParams) && !empty($reqQryParams)) {
            //create query
            $tbl1 = $reqQryParams["tables"][0];
            $tbl2 = $reqQryParams["tables"][1];
            $selectCluseCondtn = "*";
            $onClauseCondtn = "";
            $whereClauseCondtn = "";
            $orderByClauseCondtn = "";
            
            if (!empty($reqQryParams["selectField"]))
                $selectCluseCondtn = $reqQryParams["selectField"];

            if (!empty($reqQryParams["onCondition"]))
                $onClauseCondtn = "ON ".$reqQryParams["onCondition"];
            
            if (!empty($reqQryParams["whereCondition"]))
                $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

            if (!empty($reqQryParams["orderByCondtn"]))
                $orderByClauseCondtn = "ORDER BY  ".$reqQryParams["orderByCondtn"];           

            $qry = "SELECT $selectCluseCondtn FROM $tbl1 LEFT JOIN $tbl2 $onClauseCondtn $whereClauseCondtn $orderByClauseCondtn";
            $prepareQry = $this->dbconn->prepare($qry);
            if (!empty($qryParams) && count($qryParams) > 0) {
                $execQry = $prepareQry->execute($qryParams);
            } else {
                $execQry = $prepareQry->execute();
            }
            $rowCnt = $prepareQry->rowCount();
            $fetchData = array();
            if ($rowCnt > 0 ) {
                if (strtolower($reqQryParams["fetchType"]) == "singlerow")
                    $fetchData = $prepareQry->fetch(PDO::FETCH_ASSOC);
                else
                    $fetchData = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
            }
            return $fetchData;
        }
    }

    /* Function to execulte the Leftjoin query */
    public function funExeFourTblLeftJoinQuery($reqQryParams, $qryParams = array()) {

        if (is_array($reqQryParams) && !empty($reqQryParams)) {

            //create query
            $tbl1 = $reqQryParams["tables"][0];
            $tbl2 = $reqQryParams["tables"][1];
            $tbl3 = $reqQryParams["tables"][2];
            $tbl4 = $reqQryParams["tables"][3];
            $onCond1 = "ON ".$reqQryParams["onCondition"][0];
            $onCond2 = "ON ".$reqQryParams["onCondition"][1];
            $onCond3 = "ON ".$reqQryParams["onCondition"][2];
        
            $selectCluseCondtn = "*";
            $onClauseCondtn = "";
            $whereClauseCondtn = "";
            $orderByClauseCondtn = "";
            

            if (!empty($reqQryParams["selectField"]))
                $selectCluseCondtn = $reqQryParams["selectField"];
            
            if (!empty($reqQryParams["whereCondition"]))
                $whereClauseCondtn = "WHERE ".$reqQryParams["whereCondition"]; 

            if (!empty($reqQryParams["orderByCondtn"]))
                $orderByClauseCondtn = "ORDER BY  ".$reqQryParams["orderByCondtn"];           

            $qry = "SELECT $selectCluseCondtn FROM $tbl1 INNER JOIN $tbl2 $onCond1 INNER JOIN $tbl3 $onCond2 LEFT JOIN $tbl4 $onCond3 $whereClauseCondtn $orderByClauseCondtn";
            $prepareQry = $this->dbconn->prepare($qry);
            if (!empty($qryParams) && count($qryParams) > 0) {
                $execQry = $prepareQry->execute($qryParams);
            } else {
                $execQry = $prepareQry->execute();
            }
            $rowCnt = $prepareQry->rowCount();
            $fetchData = array();
            if ($rowCnt > 0 ) {
                if (strtolower($reqQryParams["fetchType"]) == "singlerow")
                    $fetchData = $prepareQry->fetch(PDO::FETCH_ASSOC);
                else
                    $fetchData = $prepareQry->fetchAll(PDO::FETCH_ASSOC);
            }
            return $fetchData;
        }
    }

    public function funAuthentication($tableName,$AccessToken) {

        $selQryParams = array (":access_token" => $AccessToken);
        $whereCondtn = $this->funParseQryParams($selQryParams, "access_token"); 
        $reqQryParams = array (
                "fetchType" => "singleRow",
                "selectField" => "*",
                "tableName" => "pro_authentications",
                "whereCondition" => $whereCondtn
            );
        $userInfo = $this->funExeSelectQuery($reqQryParams, $selQryParams);
        if ($userInfo) {
            return true;
        } else {
            return false;
        }
    }

    /*Function return JSON results from given Array*/
    public function jsonResponse($jsonEncodeArr) {
        if (is_array($jsonEncodeArr)) 
            $responseJson = json_encode($jsonEncodeArr);
        return $responseJson;
    }

    // Convert base64image into image and move to assigned path
    public function base64toImage($base64image,$floderName) {
        if (!empty($base64image)) {
            $uploadDoc = "";
            if (!empty($base64image)) {

                $pos  = strpos($base64image, ';');
                if (!empty($pos)) {
                    $mimeType = explode(':', substr($base64image, 0, $pos))[1];
                    $imgData = str_replace('data:image/'.$mimeType.';base64,', '', $base64image);
                } else {
                    $imgData = str_replace('data:image/png;base64,', '', $base64image);
                }
                $imgData = str_replace(' ', '+', $imgData);
                $imgData = base64_decode($imgData);
                $randNumber = sha1($this->generateRandAlphanumeric(24));
                $genRandFilename = '../uploads/'.$floderName."/".$randNumber. '.png';               
                $moveImgpath = file_put_contents($genRandFilename, $imgData);
                if ($moveImgpath)
                    $uploadDoc = 'uploads/'.$floderName."/".$randNumber. '.png';
            }
            return $uploadDoc;
        }
    }

    // Convert base64documents into documents and move to assigned path
    public function base64toDocs($base64docs,$floderName) {        
        if (!empty($base64docs)) {
            $uploadDoc = "";
            $type = pathinfo(base64_decode($base64docs), PATHINFO_EXTENSION);
            $imgData = base64_decode($base64docs);
            $randNumber = sha1($this->generateRandAlphanumeric(24));
            $genRandFilename = '../uploads/'.$floderName."/".$randNumber.".".$type;
            if(copy($imgData,$genRandFilename))
                $uploadDoc = 'uploads/'.$floderName."/".$randNumber.".".$type;            
            return $uploadDoc;
        }
    }

    // Select userInfo
    public function getUserInfo($userInfoArr) {

        if (is_array($userInfoArr) && count($userInfoArr) > 0) {

            $selQryParams = array (":user_id" => $userInfoArr["userId"], ":user_type" => $userInfoArr["userType"]);
            $whereCondtn = $this->funParseQryParams($selQryParams, "user_type", "AND");
            $reqQryParams = array (
                "fetchType" => "singleRow",
                "selectField" => "*",
                "tableName" => "tbl_users",
                "whereCondition" => $whereCondtn
            );
            $userInfoRes = $this->funExeSelectQuery($reqQryParams, $selQryParams);
            return $userInfoRes;
        }
        
    }

    public function generateRandAlphanumeric() {
        // return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        return rand(10,100).sprintf( '%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0C2f ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0x2Aff ), mt_rand( 0, 0xffD3 ), mt_rand( 0, 0xff4B )
        ).rand(10,100);

    }

    public function generateVerficationCode($digits) {
       return $verfication_code=rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

    /*Encryption and Decryption Password function start Here*/
    var $skey = "a2c4e6g8i0a2c4e6g8i02017"; // change this
 
    public function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
    
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }
 
    public function encode($value) { 
        if (!$value) { return false; }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext)); 
    }
 
    public function decode($value) {
        if (!$value) { return false; }
        $crypttext = $this->safe_b64decode($value); 
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }
    /*Encryption and Decryption Password function end Here*/

    // Send email
    public function sendEmailNotification($mailParams) {

        require '../includes/PHPmailer/PHPMailerAutoload.php';
        
        $mail = new PHPMailer;
        $mail->Host = 'shenll.net;shenll.net'; // Specify main and backup server
        $mail->From = $mailParams['fromAddress'];
        $mail->FromName = 'BUSINESS';     
        $mail->addAddress($mailParams['toAddress'], '');
        $mail->isHTML(true);                                
        $mail->Subject = $mailParams['subject'];
        $mail->Body = $mailParams['bodyMsg'];
        $mailStatus = false;
        if($mail->send()){
            $mailStatus = true;
        }
        return $mailStatus;
    }
}
?>
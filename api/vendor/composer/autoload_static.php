<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit4ae13607a241b4b45d6e9136d937c296
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Twilio\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Twilio\\' => 
        array (
            0 => __DIR__ . '/..' . '/twilio/sdk/Twilio',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit4ae13607a241b4b45d6e9136d937c296::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit4ae13607a241b4b45d6e9136d937c296::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}

<?php    
include_once('./includes/configure.php');
include_once('./api/Common.php');
$commonAppApi = new Common($dbconn);

$alertclass = "";
$loginerror_message = "";
if (isset($_POST['first_name']) && ($_POST['first_name']) != "" && isset($_POST['last_name']) && $_POST['last_name']!="" && isset($_POST['email']) && $_POST['email']!="" && isset($_POST['phone']) && $_POST['phone']!="") {
	if ($commonAppApi->valid_email($_POST['email'])) {

		$selQryParams = array (":email" =>trim($_POST['email']), ":status" => 1);
		$whereCondtn = $commonAppApi->funParseQryParams($selQryParams, "status", "AND"); 
		$reqQryParams = array (
							"fetchType" => "singlerow",
							"selectField" => "count(email) as countRows, user_id",
							"tableName" => "tbl_users",
							"whereCondition" => $whereCondtn);
		$getContactRes = $commonAppApi->funExeSelectQuery($reqQryParams, $selQryParams);

		if ($getContactRes["countRows"] == 0 && $getContactRes["user_id"] == "") {

			$phoneSelQryParams = array (":phone" =>trim($_POST['phone']), ":status" => 1);
			$whereCondtnPhone = $commonAppApi->funParseQryParams($phoneSelQryParams, "status", "AND");
			$phoneReqQryParams = array (
									"fetchType" => "singlerow",
									"selectField" => "count(phone) as countRows, user_id",
									"tableName" => "tbl_users",
									"whereCondition" => $whereCondtnPhone);
			$getPhoneRes = $commonAppApi->funExeSelectQuery($phoneReqQryParams, $phoneSelQryParams);
			if ($getPhoneRes["countRows"] == 0 && $getPhoneRes["user_id"] == 0) {
				$insQryParams = array (
									":first" => trim($_POST["first_name"]),
									":last" => trim($_POST["last_name"]),
									":email" => trim(strtolower($_POST["email"])),
									":type" => "labour",
									":phone" => $_POST["phone"],
									":status" => 1,
									);
				$insQryResponse = $commonAppApi->funExeInsertRecord("tbl_users", $insQryParams);
				if (!empty($insQryResponse)) {
					$name = trim($_POST["first_name"])."  ".trim($_POST["last_name"]);
		            $mailParams = array(
		                        "fromAddress" => "admin@business.com",
		                        "toAddress" => "thiyagu.shenll@gmail.com",
		                        "contactName" => "THIYAGARAJAN",
		                        "subject" => "Get Job Recruitment",
		                        "bodyMsg" => "<p>Dear ".$name.",</p>
		                                    <p>Welcome Good Job...,</p>
		                                    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
		                                    <p style='line-height:5px;'>Wishing you the very best,</p> 
		                                    <p style='line-height:0px;'>Business team</p>"
		                        );
		            $sendEmail = $commonAppApi->sendEmailNotification($mailParams);

					$alertclass = "success";
					$loginerror_message = "Register! Successfully";
				} else {
					$alertclass = "danger";
					$loginerror_message = "Warning!";
				}
			} else {
				$alertclass = "danger";
				$loginerror_message = "Danger! Phone already registered";
			}
		} else {
			$alertclass = "danger";
			$loginerror_message = "Danger! Email already registered";
		}
	} else {
		$alertclass = "danger";
		$loginerror_message = "Danger! Please enter valid email";
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Admin</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Preview page of Metronic Admin Theme #2 for " name="description" />
<meta content="" name="author" />
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="./assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="./assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="./assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="./assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<link href="./assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="./assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="./assets/pages/css/login-3.min.css" rel="stylesheet" type="text/css" />
<link href="./assets/layouts/layout2/css/custom_reg.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="favicon.ico" />
</head>
<body class="login">
    <div class="col-md-12">
        <div class="col-md-7 col-sm-7 col-xs-12">
            <div class="logo vendorlogin" >
                <a href="index">
                    <img src="./assets/layouts/layout2/img/logo.png" alt="logo" class="law-pro"/> 
                </a>
            </div><br><br><br><br><br><br>
            <div class="col-md-12 col-sm-12 col-xs-12">
	            <div class="col-md-offset-1 col-sm-12 col-xs-12 bus-labours">
	               <p class="bus-rec"><span>Recruitment</span></p>
	               <p class="bus-lb"><span>for Labours</span></p>
	               <p class="bus-lr"><span>Lorem Ipsum is simply dummy</span></p>
	            </div>
	        </div>
	        <div class="col-md-12 col-sm-12 col-xs-12">
	            <div class="col-md-offset-1 col-md-8 col-sm-12 col-xs-12 bus-cont">
	               <p class="bus-content">Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
		        </div>
	        </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12">
        	<form class="register_form" method="post" id="register_form">
        		<div class="buss-register">
        			<div class="register-title">
        				<p class="active">Join us now & get <br>Immediate Job Offers</p>
        			</div>
	            	<div class="register-content">
	            		<?php
	                    if (isset($alertclass) && $alertclass != "") { ?>
	                    <div class="alert alert-<?php echo $alertclass; ?>">
	                        <button type="button" class="close" data-dismiss="alert"></button>
	                        <p><?php echo $loginerror_message; ?></p>
	                    </div>
		                <?php } ?>
	                    <div class="form-group bus-reg">
	                        <label class="control-label visible-ie8 visible-ie9">First Name</label>
	                        <div class="input-icon">
	                            <!-- <i class="fa fa-user"></i> -->
	                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="First Name" name="first_name" id="first_name"/> </div>
	                    </div>
	                    <div class="form-group">
	                        <label class="control-label visible-ie8 visible-ie9">Last Name</label>
	                        <div class="input-icon">
	                            <!-- <i class="fa fa-lock"></i> -->
	                            <input class="form-control placeholder-no-fix" type="test" autocomplete="off" placeholder="Last Name" name="last_name" id="last_name" /> </div>
	                    </div>
	                    <div class="form-group">
	                        <label class="control-label visible-ie8 visible-ie9">Email</label>
	                        <div class="input-icon">
	                            <!-- <i class="fa fa-lock"></i> -->
	                            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id='email' /> </div>
	                    </div>
	                    <div class="form-group">
	                        <label class="control-label visible-ie8 visible-ie9">Phone</label>
	                        <div class="input-icon">
	                            <!-- <i class="fa fa-lock"></i> -->
	                            <input class="form-control placeholder-no-fix numeric" type="text" autocomplete="off" placeholder="Phone" pattern="[0-9]{1,10}" name="phone" id="phone" onkeypress="phoneno()" minlength="10" maxlength="10"/> </div>
	                    </div>
	                    <div class="form-group">
	                        <button type="submit" class="btn customlogin custombtn"> Submit </button>
	                    </div>
	                    <div class="register-design">
		            		<img style="width: 100%;" src="./assets/layouts/layout2/img/bottom-design.png" class="img-responsive">
		            	</div>
	            	</div>
	            	
            	</div>
            </form>
        </div>
    </div>    
    <script src="./assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="./assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="./assets/global/scripts/app.min.js" type="text/javascript"></script>
    <script src="./assets/pages/scripts/login4.js" type="text/javascript"></script>
</body>
</html>
<script>
$("#register_form").validate({
	rules: {
		first_name: "required",
		last_name: "required",
		email: {
			required: true,
			email: true
		},
		password: {
			required: true
		},
		phone: {
			required:true,
			integer: true
        },
	},
		messages: {
		first_name: "Please enter your first name",
		last_name: "Please enter your last name",
		email: {required:"Please enter your email",email:"Please enter a valid email"},
		password: {required:"Please enter password"},
		phone: {required:"Please enter your phone number",integer:"Please enter min 10 digit phone number"},
	},
	submitHandler: function(form) {
		form.submit();
	}
});

function phoneno(){          
    $('#phone').keypress(function(e) {
        var a = [];
        var k = e.which;

        for (i = 48; i < 58; i++)
            a.push(i);

        if (!(a.indexOf(k)>=0))
            e.preventDefault();
    });
}
</script>